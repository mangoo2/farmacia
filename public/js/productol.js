var table;
var tableModal;
var idTa=1;
var typeTa=1;
$(document).ready(function($) {
	//$("#T1").hide();
	$("#T2").hide();
	$("#T3").hide();
  $("#T4").hide();
	table = $('#tableT').DataTable();
  tableModal=$("#tableop").DataTable();
    loadtableF();
    //loadtableT();
});

$("#filtro").change(function(event){
	val=$("#filtro").val();
	switch (val){
		case "1":
			$("#T2").hide();
			$("#T3").hide();
			$("#T1").show();
      $("#T4").hide();
			loadtableF();
		break;
		case "2":
			$("#T1").hide();
			$("#T3").hide();
			$("#T2").show();
      $("#T4").hide();
			loadtableT();
		break;
		case "3":
			$("#T1").hide();
			$("#T2").hide();
			$("#T3").show();
      $("#T4").hide();
			loadtableS();
		break;
    case "4":
      $("#T1").hide();
      $("#T2").hide();
      $("#T3").hide();
      $("#T4").show();
      loadtableE();
    break;
		default:
		alert("De alguna manera usted coloco una configuracion no valida recarge aver si se arregla");
		break;
	}
});

$("#CargarInf").click(function(){
  sel=$("#selector").val();
  $.ajax({
        type:'POST',
        url: ruta+'Productos/AddData',
        data: {
            'id': idTa,
            'type': typeTa,
            'select':sel,
        },
        async: false,
        statusCode:{
            404: function(data){ toastr.error('Error!', 'No Se encuentra el archivo');},
            500: function(){ toastr.error('Error', '500');}
        },
        success:function(data){
          console.log(data);
        }
    });
});

//modals operacionales
function TipoMed(id){
  $("#Operacional").modal('show');
  $("#Titulo").text('Tipo Medicamento');
  $("#etiqueta").text('Tipo');
  LoadTableEsp(id,1);
}

function Substancia(id){
  $("#Operacional").modal('show');
  $("#Titulo").text('Substancia');
  $("#etiqueta").text('Substancia');
  LoadTableEsp(id,2);
}

function Clasif(id){
  $("#Operacional").modal('show');
  $("#Titulo").text('Clasificacion Medicamento');
  $("#etiqueta").text('Clasificacion');
  LoadTableEsp(id,3);
}

function LoadTableEsp(id,d){
  $("#selector").empty();
  $.ajax({
        type:'POST',
        url: ruta+'Productos/FullSelec',
        data: {
            'type': d,
        },
        async: false,
        statusCode:{
            404: function(data){ toastr.error('Error!', 'No Se encuentra el archivo');},
            500: function(){ toastr.error('Error', '500');}
        },
        success:function(data){
          dta=JSON.parse(data);
          Complete="";
          $.each(dta,function(key, reg){
            op='<option value="'+reg.id+'">'+reg.nombre+'</option>';
            Complete=Complete+op;
          });
          $("#selector").append(Complete);
        }
    });
  idTa=id;
  typeTa=d;
  tableModal.ajax.reload();

}


function loadtableF(){
    table.destroy();
    table = $('#tableF').DataTable({
        stateSave: true,
        responsive: !0,
        "bProcessing": true,
        "serverSide": true,
        "ajax": {
            "url": ruta+"index.php/Productos/getlistproductos",
            type: "post",
        },
        "columns": [
            {"data": "productoid"},
            {"data": "nopro"},
            {"data": "nombre"},
            {"data": "precioventa"},
            {"data": "codigo"},
            {"data": "stock"},
            {"data": "productoid",
                render:function(data,type,row){
                    var html='<div class="btn-group mr-1 mb-1">\
                                  <button type="button" class="btn btn-raised btn-outline-warning"><i class="fa fa-cog"></i></button>\
                                  <button type="button" class="btn btn-raised btn-warning dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">\
                                      <span class="sr-only">Toggle Dropdown</span>\
                                  </button>\
                                  <div class="dropdown-menu" x-placement="bottom-start" style="position: absolute; transform: translate3d(84px, 38px, 0px); top: 0px; left: 0px; will-change: transform;">\
                                      <a class="dropdown-item" href="'+ruta+'Productos/productosadd/'+row.productoid+'">Editar</a>\
                                      <a class="dropdown-item producto_row_'+row.productoid+'"\
                                        onclick="etiquetas('+row.productoid+');"\
                                            data-codigo="'+row.codigo+'" \
                                            data-nombre="'+row.nombre+'" \
                                            data-categoria="'+row.categoria+'" \
                                            data-precioventa="'+row.precioventa+'" \
                                        href="#">Etiquetas</a>\
                                      <a class="dropdown-item" onclick="productodelete('+row.productoid+');"href="#">Eliminar</a>\
                                  </div>\
                              </div>';
                    return html;
                }
            },            
        ],
        "order": [[ 0, "desc" ]],
        "lengthMenu": [[25, 50, 100], [25, 50,100]],
        /*/"fnRowCallback": function( nRow, aData, iDisplayIndex, iDisplayIndexFull ) {
            //console.log(aData.stock+'<='+aData.stockmin+'----'+aData.productoid);
                       if (aData.stock<=aData.stockmin){
                           $(nRow).addClass('alertastock');
                       }
                       
                   }/*/
        
    });
}

function loadtableT(){
    table.destroy();
    table = $('#tableT').DataTable({
        stateSave: true,
        responsive: !0,
        "bProcessing": true,
        "serverSide": true,
        "ajax": {
            "url": ruta+"index.php/Productos/getlistproductosT",
            type: "post",
        },
        "columns": [
            {"data": "productoid"},
            {"data": "nombre"},
            {"data": "categoria"},
            {"data": "precioventa"},
            {"data": "codigo"},
            {"data": "stock"},
            {"data": "productoid",
                render:function(data,type,row){
                    var html='<div class="btn-group mr-1 mb-1">\
                                  <button type="button" class="btn btn-raised btn-outline-warning"><i class="fa fa-cog"></i></button>\
                                  <button type="button" class="btn btn-raised btn-warning dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">\
                                      <span class="sr-only">Toggle Dropdown</span>\
                                  </button>\
                                  <div class="dropdown-menu" x-placement="bottom-start" style="position: absolute; transform: translate3d(84px, 38px, 0px); top: 0px; left: 0px; will-change: transform;">\
                                      <a class="dropdown-item" href="'+ruta+'Productos/productosadd/'+row.productoid+'">Editar</a>\
                                      <a class="dropdown-item producto_row_'+row.productoid+'"\
                                        onclick="etiquetas('+row.productoid+');"\
                                            data-codigo="'+row.codigo+'" \
                                            data-nombre="'+row.nombre+'" \
                                            data-categoria="'+row.categoria+'" \
                                            data-precioventa="'+row.precioventa+'" \
                                        href="#">Etiquetas</a>\
                                      <a class="dropdown-item" onclick="productodelete('+row.productoid+');"href="#">Eliminar</a>\
                                  </div>\
                              </div>';
                    return html;
                }
            },            
        ],
        "order": [[ 0, "desc" ]],
        "lengthMenu": [[25, 50, 100], [25, 50,100]],
        /*/"fnRowCallback": function( nRow, aData, iDisplayIndex, iDisplayIndexFull ) {
            //console.log(aData.stock+'<='+aData.stockmin+'----'+aData.productoid);
                       if (aData.stock<=aData.stockmin){
                           $(nRow).addClass('alertastock');
                       }
                       
                   }/*/
        
    });
}

function loadtableS(){
    table.destroy();
    table = $('#tableS').DataTable({
        stateSave: true,
        responsive: !0,
        "bProcessing": true,
        "serverSide": true,
        "ajax": {
            "url": ruta+"index.php/Productos/getlistproductosS",
            type: "post",
        },
        "columns": [
            {"data": "productoid"},
            {"data": "codigo"},
            {"data": "nombre"},
            {"data": "productoid",
                render:function(data,type,row){
                    var html='<div class="btn-group mr-1 mb-1">\
                                  <button type="button" class="btn btn-raised btn-outline-warning"><i class="fa fa-cog"></i></button>\
                                  <button type="button" class="btn btn-raised btn-warning dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">\
                                      <span class="sr-only">Toggle Dropdown</span>\
                                  </button>\
                                  <div class="dropdown-menu" x-placement="bottom-start" style="position: absolute; transform: translate3d(84px, 38px, 0px); top: 0px; left: 0px; will-change: transform;">\
                                      <a class="dropdown-item" href="'+ruta+'Productos/productosadd/'+row.productoid+'">Editar</a>\
                                      <a class="dropdown-item producto_row_'+row.productoid+'"\
                                        onclick="etiquetas('+row.productoid+');"\
                                            data-codigo="'+row.codigo+'" \
                                            data-nombre="'+row.nombre+'" \
                                            data-categoria="'+row.categoria+'" \
                                            data-precioventa="'+row.precioventa+'" \
                                        href="#">Etiquetas</a>\
                                      <a class="dropdown-item" onclick="productodelete('+row.productoid+');"href="#">Eliminar</a>\
                                  </div>\
                              </div>';
                    return html;
                }
            },            
        ],
        "order": [[ 0, "desc" ]],
        "lengthMenu": [[25, 50, 100], [25, 50,100]],
        /*/"fnRowCallback": function( nRow, aData, iDisplayIndex, iDisplayIndexFull ) {
            //console.log(aData.stock+'<='+aData.stockmin+'----'+aData.productoid);
                       if (aData.stock<=aData.stockmin){
                           $(nRow).addClass('alertastock');
                       }
                       
                   }/*/
        
    });
}

function loadtableE(){
    table.destroy();
    table = $('#tableE').DataTable({
        stateSave: true,
        responsive: !0,
        "bProcessing": true,
        "serverSide": true,
        "ajax": {
            "url": ruta+"index.php/Productos/getlistproductosE",
            type: "post",
        },
        "columns": [
            {"data": "productoid"},
            {"data": "codigo"},
            {"data": "nombre"},
            {"data": "tipopro",
                render:function(data,type,row){
                    ret="";
                    switch (data){
                      case "1":
                          ret="Farmacia";
                        break;
                      case "2":
                          ret="Tienda";
                        break;
                      default:
                          ret="Algo salio horriblemente mal";
                        break;
                    }
                    return ret;
                }
            },
            {"data": "precioventa"},
            {"data": "stock"},
            {"data": "productoid",
                render:function(data,type,row){
                    var html='<div class="btn-group mr-1 mb-1">\
                                  <button type="button" class="btn btn-raised btn-outline-warning"><i class="fa fa-cog"></i></button>\
                                  <button type="button" class="btn btn-raised btn-warning dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">\
                                      <span class="sr-only">Toggle Dropdown</span>\
                                  </button>\
                                  <div class="dropdown-menu" x-placement="bottom-start" style="position: absolute; transform: translate3d(84px, 38px, 0px); top: 0px; left: 0px; will-change: transform;">\
                                      <a class="dropdown-item" href="'+ruta+'Productos/productosadd/'+row.productoid+'">Editar</a>\
                                  </div>\
                              </div>';
                    return html;
                }
            },            
        ],
        "order": [[ 0, "desc" ]],
        "lengthMenu": [[25, 50, 100], [25, 50,100]],
        /*/"fnRowCallback": function( nRow, aData, iDisplayIndex, iDisplayIndexFull ) {
            //console.log(aData.stock+'<='+aData.stockmin+'----'+aData.productoid);
                       if (aData.stock<=aData.stockmin){
                           $(nRow).addClass('alertastock');
                       }
                       
                   }/*/
        
    });
}