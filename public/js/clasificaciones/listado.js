var table;
$(document).ready(function($) {
	table = $('#data-tables').DataTable();
	loadtable();
});

function loadtable(){
    table.destroy();
    table = $('#data-tables').DataTable({
        stateSave: true,
        responsive: !0,
        "bProcessing": true,
        "serverSide": true,
        "ajax": {
            "url": ruta+"Clasificaciones/getClas",
            type: "post",
        },
        "columns": [
            {"data": "id"},
            {"data": "nombre"},
            {"data": "id",
                render:function(data,type,row){
                    var html='<div class="btn-group mr-1 mb-1">\
                                  <button type="button" class="btn btn-raised btn-outline-warning"><i class="fa fa-cog"></i></button>\
                                  <button type="button" class="btn btn-raised btn-warning dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">\
                                      <span class="sr-only">Toggle Dropdown</span>\
                                  </button>\
                                  <div class="dropdown-menu" x-placement="bottom-start" style="position: absolute; transform: translate3d(84px, 38px, 0px); top: 0px; left: 0px; will-change: transform;">\
                                      <a class="dropdown-item" href="'+ruta+'Clasificaciones/alta/'+row.id+'">Editar</a>\
                                      <a class="dropdown-item" onclick="delete_record('+row.id+');"href="#">Eliminar</a>\
                                  </div>\
                              </div>';
                    return html;
                }
            },         
        ],
        "order": [[ 0, "desc" ]],
        "lengthMenu": [[25, 50, 100], [25, 50,100]],
    });
}

function delete_record(id) {
        swal({
            title: "¿Desea eliminar este tipo?",
            text: "Se eliminará definitivamente del listado",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Eliminar",
            closeOnConfirm: false
        }).then(function (isConfirm) {
            if (isConfirm) {
               $.ajax({
                     type: "POST",
                     url: base_url+"Clasificaciones/delete/"+id,
                     success: function (result) {
                        console.log(result);
                        table.ajax.reload();
                        swal("Éxito!", "Se ha eliminado correctamente", "success");
                     }
                 });
            }
        });

    }