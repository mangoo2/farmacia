$(document).ready(function(){
	$('#chkFecha').change(function(){
		if(document.getElementById("chkFecha").checked){
			f = new Date();
			if(f.getDate()<10){
				dia = "0"+f.getDate();
			}else{dia = f.getDate();
			}
			if(f.getMonth()<10){
				mes = "0"+(f.getMonth()+1);
			}else{
				mes = (f.getMonth+1);
			}
			fecha = f.getFullYear()+"-"+mes+"-"+dia;
			$('#txtInicio').val(fecha);
			$('#txtFin').val(fecha);
			$('#txtInicio').attr('disabled','disabled');
			$('#txtFin').attr('disabled','disabled');
			
			
		}
		else{
			$('#txtInicio').val('');
			$('#txtFin').val('');
			$('#txtInicio').removeAttr('disabled');
			$('#txtFin').removeAttr('disabled');
		}
	});
	$('#btnBuscar').click(function(){
		params = {};
		params.fecha1 = $('#txtInicio').val();
		params.fecha2 = $('#txtFin').val();
		params.personal = $('#personal option:selected').val();

		if(params.fecha1 != '' && params.fecha2 != ''){
			$.ajax({
				type:'POST',
				url:'Corte_caja/corte',
				data:params,
				async:false,
				success:function(data){
					//console.log(data);
					var array = $.parseJSON(data);
					$('#tbCorte').html(array.tabla);
					//$('#tbCorte2').html(array.tabla2);
					$('.tbCorteVC').html(array.tabla3);
					$('.reportetablemayor').html(array.tabla5);
					$('#rowventas').html(array.totalventas);
					$('#totalutilidades').html(array.totalutilidad);
					$('#totalefectivo').html(array.totalefectivo);
					$('#totaltarjetas').html(array.totaltarjetas);
					$('#totalGral').html(array.totalGral);
					/*$('#cRealizadas').html(array.cRealizadas);
					$('#cContado').html(array.cContado);
					$('#cCredito').html(array.cCredito);
					$('#cTerminal').html(array.cTerminal);
					$('#cCheque').html(array.cCheque);
					$('#cTransferencia').html(array.cTransferencia);
					$('#cPagos').html(array.cPagos);
					$('#dContado').html('$ '+array.dContado);
					$('#dCredito').html('$ '+array.dCredito);
					$('#dTerminal').html('$ '+array.dTerminal);
					$('#dCheque').html('$ '+array.dCheque);
					$('#dTransferencia').html('$ '+array.dTransferencia);
					$('#dPagos').html('$ '+array.dPagos);*/
					$('#dTotal').html(''+array.dTotal);
					$('#dTotalvc').html(''+array.dTotalvc);
                    //$('#dImpuestos').html(''+array.dImpuestos);
                    $('#sample_1').DataTable({dom: 'Bfrtip',
                            buttons: [
                                    //{extend: 'excel',title: 'Reporte de saldos'},
                                    {extend: 'excel'},
                                    {extend: 'pdf'},
                                    {extend:'print'},
                                    'pageLength'
                            ]});
                    /*$('#sample_2').DataTable({dom: 'Bfrtip',
                            buttons: [
                                    //{extend: 'excel',title: 'Reporte de saldos'},
                                    {extend: 'excel'},
                                    {extend: 'pdf'},
                                    {extend:'print'},
                                    'pageLength'
                            ]});*/

                    $('#sample_3').DataTable({dom: 'Bfrtip',
                            buttons: [
                                    //{extend: 'excel',title: 'Reporte de saldos'},
                                    {extend: 'excel'},
                                    {extend: 'pdf'},
                                    {extend:'print'},
                                    'pageLength'
                            ]});

                    $('#sample_5').DataTable({dom: 'Bfrtip',
                            buttons: [
                                    //{extend: 'excel',title: 'Reporte de saldos'},
                                    {extend: 'excel'},
                                    {extend: 'pdf'},
                                    {extend:'print'},
                                    'pageLength'
                            ]});

				}
			});
		}
		else{
			toastr.error('No existen fechas validas', 'Error');
			
		}
	});
});