
$(document).ready(function(){

  $("#prod_search").autocomplete({
  	delay: 500,
    source: function (request, response) {
      $.ajax({
        url: base_url+'Verificador/getDatosProd',
        type: 'POST',
        dataType: "json",
        data: { prod: $("#prod_search").val() },
        success: function(json) {
          response($.map(json, function(item) {
            return {
              value: item.codigo+" - "+item.nombre + " / "+item.substancia,
              nombre: item.nombre,
              productoid: item.productoid,
              substancia: item.substancia,
              idsubstancia: item.idsubstancia,
              precio: item.precioventa,
              stock: item.stock,
              codigo: item.codigo
            }
            //$('#prod_search').val(ui.item.codigo+" - "+ui.item.value);
          }));
        }
      });
    }, 
    select: function(event, ui) { 
      $('#name').html("Nombre: "+ui.item.nombre);
      $('#subs').html("Substancia: "+ui.item.substancia);
      var precio=new Intl.NumberFormat('es-MX',{style: 'currency', currency: 'MXN'}).format(ui.item.precio);
      $('#pre').html("Precio: <strong>"+precio+"</strong>");
      $('#exis').html("Existencia: "+ui.item.stock);

      mostrarSimilares(ui.item.productoid, ui.item.substancia, ui.item.idsubstancia);
    }, 
    focus: function(event, ui) {
      return false;
    }
  });

});

function mostrarSimilares(id,subs,idsubs){
  $("#cont_search").show();
  table = $('#tabla_tipos').DataTable({
      destroy: true,
    "pagingType": "full_numbers",
    "processing": true,
    "scrollX": true,
    "ajax": {
       "url": base_url+"Verificador/datatable_records",
       type: "post",
       data: { id:id, substancia:subs, idsubstancia:idsubs }
    },
    "pageLength": 10,
    "columns": [
        {"data": "nombre"},
        {"data": "precioventa"},
        //{"data": "presentacion"},
        {"data": "stock"},
        {"data": "substancia"},
    ],
     rowCallback: function (row, data) {
        $(row).addClass('font-small-2');
     },
     "order": [[ 0, "desc" ]],
  });
  
}