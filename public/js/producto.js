var base_url = $('#base_url').val();
var cont1=0;
var cont2=0;
var cont3=0;
var campoval="";
var typeTa=1;
var tableModal;
$(document).ready(function () {
    $("#ServiceS").hide();
    $("#ProductST").hide();
    $("#poveedoresT").hide();
    tableModal=$("#tableop").DataTable();
    LoadTM();
        var form_register = $('#formproductos');
        var error_register = $('.alert-danger', form_register);
        var success_register = $('.alert-success', form_register);
        var $validator1=form_register.validate({
            errorElement: 'div', //default input error message container
            errorClass: 'vd_red', // default input error message class
            focusInvalid: false, // do not focus the last invalid input
            ignore: "",
            rules: {
                pcodigo:{
                  required: true
                },
                productoname:{
                    required: true
                },
                txtUsuario:{
                    required: true
                },
                pstock:{
                    min: 0,
                    required: true
                },
                preciocompra:{
                    required: true
                },
                porcentaje:{
                    required: true
                },
                precioventa:{
                    required: true
                }  
            },            
            errorPlacement: function(error, element) {
                if (element.parent().hasClass("vd_checkbox") || element.parent().hasClass("vd_radio")){
                    element.parent().append(error);
                } else if (element.parent().hasClass("vd_input-wrapper")){
                    error.insertAfter(element.parent());
                }else {
                    error.insertAfter(element);
                }
            }, 
            invalidHandler: function (event, validator) { //display error alert on form submit              
                    success_register.fadeOut(500);
                    error_register.fadeIn(500);
                    scrollTo(form_register,-100);
            },
            highlight: function (element) { // hightlight error inputs
                $(element).addClass('vd_bd-red');
                $(element).siblings('.help-inline').removeClass('help-inline fa fa-check vd_green mgl-10');
            },
            unhighlight: function (element) { // revert the change dony by hightlight
                $(element)
                    .closest('.control-group').removeClass('error'); // set error class to the control group
            },
            success: function (label, element) {
                label
                    .addClass('valid').addClass('help-inline fa fa-check vd_green mgl-10') // mark the current input as valid and display OK icon
                    .closest('.control-group').removeClass('error').addClass('success'); // set success class to the control group
                $(element).removeClass('vd_bd-red');
            }
        });
        GetShower();
        $(".selectorM").select2();
        //GetAllprove();
        //GetAllsimi();
});

function Operacion(T){
    campoval=T.id;
}

function LoadTM(){
  tableModal.destroy();
  tableModal=$("#tableop").DataTable({
      "processing": true,
      "scrollX": true,
       "ajax":{
          url :ruta+"index.php/Productos/GetDataCatalogo",
          type: "post",
          data: function(d){
            d.type = typeTa;
          },
          },
           "pageLength": 10,
           "columns":[
           {"data": "nombre"},
           {"data": "id",
                render:function(data,type,row){
                    var html='<div class="btn-group mr-1 mb-1">\
                                <a class="btn btn-success" onclick="EditCat('+data+')"><i class="fa fa-edit"></i></a>\
                                <a class="btn btn-danger" onclick="DeleteCat('+data+')"><i class="fa fa-trash"></i></a>\
                              </div>';
                    return html;
                }
              },
           ],
           rowCallback: function (row, data) {
              $(row).addClass('font-small-2');
           },
           "order": [[ 0, "desc" ]],
  });
}

function EditCat(id){
    $("#ModalEdit").modal('show');
}

function DeleteCat(d){
    $.ajax({
        type:'POST',
        url: base_url+'Productos/DelCatEle',
        data: {
            'ide':d,
            'type':typeTa,
        },
        async: true,
        success:function(d){
            toastr.success('Hecho!', 'Borrado Correctamente');
        }
    });
    LoadTableEsp(typeTa);
    FullAllSel();
}

function GetShower(){
  val=$("#productype").val();
  switch (val){
    case '1':
      $("#ProductSF").show();
      $("#poveedoresF").show();
      $("#Similares").show();
      $("#poveedoresT").hide();
      $("#ProductST").hide();
      GetAllprove();
      //GetAllsimi();
      break;
    case '2':
      $("#ProductST").show();
      $("#poveedoresT").show();
      $("#poveedoresF").hide();
      $("#ProductSF").hide();
      $("#Similares").hide();
      GetAllproveT();
      break;
    case '3':
      $("#ServiceS").show();
      $("#ProductSF").hide();
      $("#poveedoresF").hide();
      $("#Similares").hide();
      $("#poveedoresT").hide();
      $("#ProductST").hide();
      break;
    default:
      break;
  }
}

function Moneda(T){
    valor = T.value;
    N=new Intl.NumberFormat('es-MX', {style: 'currency',currency: 'MXN',minimumFractionDigits: 2}).format(valor);
    T.value=N;

}

$("#tpp").change(function(event){
    val=$("#tpp").val();
    if (val==1){
        $("#ProductSF").show();
        $("#poveedoresF").show();
        $("#Similares").show();
        $("#poveedoresT").hide();
        $("#ProductST").hide();
    }else{
        $("#ProductST").show();
        $("#poveedoresT").show();
        $("#poveedoresF").hide();
        $("#ProductSF").hide();
        $("#Similares").hide();
    }
});

$("#CS").click(function(event) {
    if ($("#CS").is(':checked')){
        $("#ServiceS").show();
        $("#ProductSF").hide();
        $("#poveedoresF").hide();
        $("#Similares").hide();
        $("#poveedoresT").hide();
        $("#ProductST").hide();
    }else{
        $("#ServiceS").hide();
        $("#ProductSFT").show();
    }
});

$('#savepr').click(function(event) {
    val=$("#tpp").val();
    if ($("#CS").is(':checked')){
        Saveservicio();
    }else{
        if (val==1){
            SaveData();
        }else{
            SaveDataT();
        }
    }
    toastr.success('Hecho!', 'Guardado Correctamente');
    setInterval(function(){ 
        location.href=ruta+'/Productos'; 
        window.location.href = ruta+"/Productos";
    }, 3000);
});

function Saveservicio(){
  var $valid = $("#formServices").valid();
  console.log($valid);
    if($valid){
        info=$("#formServices").serialize();
        $.ajax({
            type:'POST',
            url: base_url+'Productos/productoadd',
            data: info+"&tipopro=3&id="+$("#productoid").val(),
            async: false,
            success:function(d){
            }
        });
    }
}

function SaveData(){
var $valid = $("#formproductos").valid();
console.log($valid);
    if($valid){
        info=$("#formproductos").serialize();
        $.ajax({
            type:'POST',
            url: base_url+'Productos/productoadd',
            data: info+"&tipopro=1&id="+$("#productoid").val(),
            async: false,
            success:function(d){
                SaveProv(d);
                SaveSimi(d);
            }
        });
    }
}

function SaveDataT(){
var $valid = $("#formtienda").valid();
console.log($valid);
    if($valid){
        info=$("#formtienda").serialize();
        $.ajax({
            type:'POST',
            url: base_url+'Productos/productoadd',
            data: info+"&tipopro=2&id="+$("#productoid").val(),
            async: false,
            success:function(d){
                SaveProvT(d);
            }
        });
    }
}

function SaveProv(id){
    for (var i = 0; i <= cont1; i++){
        info=$("#FRMP-"+i).serialize();
        $.ajax({
            type:'POST',
            url: base_url+'Productos/SaveRelProdProv',
            data: info+"&id_producto="+id,
            async: false,
            success:function(d){
            }
        });
    }
}

function SaveSimi(id){
    info=$("#FSF-0").serialize();
    $.ajax({
        type:'POST',
        url: base_url+'Productos/SaveRelProdSim',
        data: info+"&id_producto="+id,
        async: false,
        success:function(d){
        }
    });
}

function SaveProvT(id){
    for (var i = 0; i <= cont3; i++){
        info=$("#TRMP-"+i).serialize();
        $.ajax({
            type:'POST',
            url: base_url+'Productos/SaveRelProdProv',
            data: info+"&id_producto="+id,
            async: false,
            success:function(d){
            }
        });
    }
}

function CloneElement(d1,d2,d3){
    cont1=cont1+1;
    template=$("#FRMP-0");
    clone = template.clone();
    clone.removeAttr('id');
    clone.attr('id',"FRMP-"+cont1);
    clone[0].reset();
    clone[0][0].value=d1;
    clone[0][1].value=d2;
    clone[0][2].value=d3;
    $("#CFPF").append(clone);
}

function DeletePF(T){
    F=T.form;
    fid=F.id;
    DellRelPPF(F.idd.value);
    $("#"+fid).remove();
    sep=fid.split("-");
    num=parseInt(sep[1]);
    if(num<cont1){
        console.log("entro");
        for(i=num; i<=cont1; i++){
            $("#FRMP-"+i).attr('id',"FRMP-"+(i-1));
        }
    }else{
    }
    cont1=cont1-1;
}

function CloneSimilar(d1,d2) {
    cont2=cont2+1;
    template=$("#FSF-0");
    clone = template.clone();
    clone.removeAttr('id');
    clone.attr('id',"FSF-"+cont2);
    clone[0].reset();
    clone[0][0].value=d1;
    clone[0][1].id="ES"+cont2;
    clone[0][1].value=d2;
    $("#CSF").append(clone);
}

function DeleteS(T){
    F=T.form;
    fid=F.id;
    DellSimi(F.idd.value);
    $("#"+fid).remove();
    sep=fid.split("-");
    num=parseInt(sep[1]);
    if(num<cont2){
        for(i=num; i<=cont2; i++){
            $("#CSF-"+i).attr('id',"CSF-"+(i-1));  
        }
    }else{
    }
    cont2=cont2-1;
}

function TipoMed(){
  $("#Operacional").modal('show');
  $("#Titulo").text('Tipo Medicamento');
  $("#etiqueta").text('Tipo');
  LoadTableEsp(1);
}

function Substancia(){
  $("#Operacional").modal('show');
  $("#Titulo").text('Substancia');
  $("#etiqueta").text('Substancia');
  LoadTableEsp(2);
}

function Clasif(){
  $("#Operacional").modal('show');
  $("#Titulo").text('Clasificación Medicamento');
  $("#etiqueta").text('Clasificación');
  LoadTableEsp(3);
}

function CatProT(){
  $("#Operacional").modal('show');
  $("#Titulo").text('Categoría Producto');
  $("#etiqueta").text('Categoría');
  LoadTableEsp(4);
}

function CloneElementPT(d1,d2,d3){
    cont3=cont3+1;
    template=$("#TRMP-0");
    clone = template.clone();
    clone.removeAttr('id');
    clone.attr('id',"TRMP-"+cont3);
    clone[0].reset();
    clone[0][0].value=d1;
    clone[0][1].value=d2;
    clone[0][2].value=d3;
    $("#CTPT").append(clone);
}

function DeletePT(T){
    F=T.form;
    fid=F.id;
    DellRelPPF(F.idd.value);
    $("#"+fid).remove();
    sep=fid.split("-");
    num=parseInt(sep[1]);
    if(num<cont3){
        for(i=num; i<=cont3; i++){
            $("#TRMP-"+i).attr('id',"TRMP-"+(i-1));  
            //console.log(i-1);
        }
        //console.log(i);
    }else{
    }
    cont3=cont3-1
}

function GetAllprove(){
    iden=$("#productoid").val();
    if (iden!=''){
        ExcetGetProveF(iden);
    }else{
        //aqui no
    }
}

function ExcetGetProveF(id){
    $.ajax({
      type:'POST',
      url: base_url+'Productos/GetRelProdProv',
      data: {
        "id_producto":id,
      },
      async: true,
      success:function(d){
        var data= JSON.parse(d);
        $.each(data,function(key,dato){
          CloneElement(dato.id,dato.id_proveedor,dato.precio);
        }); 
      }
    });
}

function GetAllsimi(){
    iden=$("#productoid").val();
    if (iden!=''){
        ExcetGetSimi(iden);
    }else{
        //aqui no
    }
}

function ExcetGetSimi(id){
    $.ajax({
      type:'POST',
      url: base_url+'Productos/GetSimi',
      data: {
        "id_producto":id,
      },
      async: true,
      success:function(d){
        var data= JSON.parse(d);
        $.each(data,function(key,dato){
          CloneSimilar(dato.id,dato.similar);
        }); 
      }
    });
}

function GetAllproveT(){
    iden=$("#productoid").val();
    if (iden!=''){
        ExcetGetProveT(iden);
    }else{
        //aqui no
    }
}

function ExcetGetProveT(id){
    $.ajax({
      type:'POST',
      url: base_url+'Productos/GetRelProdProv',
      data: {
        "id_producto":id,
      },
      async: true,
      success:function(d){
        var data= JSON.parse(d);
        $.each(data,function(key,dato){
          CloneElementPT(dato.id,dato.id_proveedor,dato.precio);
        }); 
      }
    });
}

function DellRelPPF(id){
  if (id>0){
    $.ajax({
      type:'POST',
      url: base_url+'Productos/DeletePPF',
      data: {
        "id":id,
      },
      async: true,
      success:function(data){
        console.log("borrado"); 
      }
    });
  }else{
    console.log("nada");
  }
}

function DellSimi(id){
  if (id>0){
    $.ajax({
      type:'POST',
      url: base_url+'Productos/DeleteSimi',
      data: {
        "id":id,
      },
      async: true,
      success:function(data){
        console.log("borrado"); 
      }
    });
  }else{
    console.log("nada");
  }
}

function calcularF(T){
	var costo = $('#preciocompraF').val();
    var porcentaje = $('#porcentajeF').val();
    var porcentaje2 = porcentaje/100;
    var costo2 = costo*porcentaje2;
    var cantitotal = parseFloat(costo)+parseFloat(costo2);
    $('#precioventaF').val(cantitotal);
    $('#preciomayoreoF').val(cantitotal);
    $('#cpmayoreoF').val('1');
}

function calcularT(T){
    var costo = $('#preciocompraT').val();
    var porcentaje = $('#porcentajeT').val();
    var porcentaje2 = porcentaje/100;
    var costo2 = costo*porcentaje2;
    var cantitotal = parseFloat(costo)+parseFloat(costo2);
    $('#precioventaT').val(cantitotal);
    $('#preciomayoreoT').val(cantitotal);
    $('#cpmayoreoT').val('1');
}

function LoadTableEsp(d){
  typeTa=d;
  tableModal.ajax.reload();
}

function FullAllSel(){
    idsel="";
    switch (typeTa){
        case 1:
        idsel="tipmed";
        break;
        case 2:
        idsel="subsmed";  
        break;
        case 3:
        idsel="clamed";
        break;
        case 4:
        idsel="catpro";
        break;
        default:
        break;
    }
    $("#"+idsel).empty();
    $.ajax({
        type:'POST',
        url: ruta+'Productos/FullSelec',
        data: {
            'type': typeTa,
        },
        async: false,
        success:function(data){
          dta=JSON.parse(data);
          Complete="";
          $.each(dta,function(key, reg){
            op='<option value="'+reg.id+'">'+reg.nombre+'</option>';
            Complete=Complete+op;
          });
        }
    });
    $("#"+idsel).append(Complete);
}

$("#CargarInf").click(function(){
  sel=$("#nombreMed").val();
  $.ajax({
        type:'POST',
        url: ruta+'Productos/AddData',
        data: {
            'type': typeTa,
            'select':sel,
        },
        async: false,
        statusCode:{
            404: function(data){ toastr.error('Error!', 'No Se encuentra el archivo');},
            500: function(){ toastr.error('Error', '500');}
        },
        success:function(data){
          toastr.success('Hecho!', 'Guardado Correctamente');
          tableModal.ajax.reload();
          $("#nombreMed").val("");
          FullAllSel();
        }
    });
});
