  $(document).ready(function () {
    /*/$('[data-toggle="tooltip"]').tooltip();
    $('#sicancelar').click(function(){
      params = {};
      params.id = $('#hddIdVenta').val();
      $.ajax({
        type:'POST',
        url:'<?php echo base_url(); ?>ListaVentas/cancalarventa',
        data:params,
        async:false,
        success:function(data){
          toastr.success('Cancelado Correctamente','Hecho!');
          //$('#trven_'+params.id).remove();
          location.reload();
        }
      });
    });/*/
    CreateT();
  });
  function CreateT(){
    tableModal=$("#data-tables").DataTable({
      "processing": true,
      "scrollX": true,
       "ajax":{
          url :ruta+"index.php/Productos/GetDataVentasC",
          type: "post",
          },
           "pageLength": 10,
           "columns":[
           {"data": "id"},
           {"data": "Nom"},
           {"data": "mont",
                 render:function(data,type,row){
                    return Moneda(data);
                }
           },
           {"data": "id",
                render:function(data,type,row){
                    var html='<button class="btn btn-raised gradient-blackberry white sidebar-shadow" onclick="ticket('+row.id+')" title="Detalle" data-toggle="tooltip" data-placement="top">\
                    <i class="fa fa-search"></i></button>';
                    return html;
                }
              },
           ],
           rowCallback: function (row, data) {
              $(row).addClass('font-small-2');
           },
           "order": [[ 0, "desc" ]],
  });
  }

  function Moneda(val){
    monto=new Intl.NumberFormat('es-MX',{style: 'currency', currency: 'MXN'}).format(val);
    console.log(monto);
    return monto;
  }

  function ticket(id){
    $("#table-venta").empty();
    $("#iframeri").modal('show');
    $.ajax({
        type:'POST',
        url: ruta+'ListaVentas/ProducList',
        data: {
            'id': id
        },
        async: false,
        statusCode:{
            404: function(data){ toastr.error('Error!', 'No Se encuentra el archivo');},
            500: function(){ toastr.error('Error', '500');}
        },
        success:function(data){
          dta=JSON.parse(data);
          Complete="";
          p1="<tr>";
          p2="</tr>";
          saldo=0;
          $.each(dta,function(key, regi){
            v1=Number(regi.monto_total);
            tbla="<td>"+regi.reg+"</td>\
            <td>"+Moneda(v1)+"</td>\
            <td><button class='btn btn-success' onclick='Pagar("+regi.id_venta+")' title='Pagar' >\
                    <i class='fa fa-money'></i></button></td>";
            saldo=saldo+v1;
            Complete=Complete+p1+tbla+p2;
          });
          $("#table-venta").append(Complete);
          $("#Saldo").text("Saldo: "+Moneda(saldo));
        }
    });
  }

  function Pagar(id){
    swal({
      title: 'Confirmar',
      text: "¿Desea saldar esta partida?",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Saldar',
      cancelButtonText: 'Cancelar'
    }).then((result) => {
      if (result){
        $.ajax({
            type:'POST',
            url: ruta+'ListaVentas/PagarVentaC',
            data: {
                'id': id
            },
            async: false,
            statusCode:{
                404: function(data){ toastr.error('Error!', 'No Se encuentra el archivo');},
                500: function(){ toastr.error('Error', '500');}
            },
            success:function(data){
              toastr.success('Hecho!', 'Accion realizada correctamente');
              location.reload();
            }
        });
      }
    })
  }

  /*//*/

  function cancelar(ID,CantidadTicket){
    $('#cancelar').modal();
    $("#hddIdVenta").val(ID);
    $("#NoTicket").html(ID);
    $("#CantidadTicket").html(CantidadTicket);  
  }
  