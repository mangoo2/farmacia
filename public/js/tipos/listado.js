
    $(document).ready(function () {
        table = $('#tabla_tipos').DataTable({
            "ajax": {
               "url": base_url+"TipoMedicamento/datatable_records",
               type: "post",
            },
            "columns": [
                {"data": "id"},
                {"data": "nombre"},
                {
                    "data": null,
                    "defaultContent": "<button id='remove' title='Editar Tipo' class='btn btn-info edit'><i class='fa fa-edit'></i></button>\
                    <button id='remove' title='Eliminar Tipo' class='btn btn-danger delete'><i class='fa fa-trash'></i></button>"
                }
            ]
        });

        $('#tabla_tipos').on('click', 'button.edit', function () {
            var tr = $(this).closest('tr');
            var row = table.row(tr);
            var data = row.data();
            window.location = base_url+'TipoMedicamento/alta/'+data.id;
        });
        $('#tabla_tipos').on('click', 'button.delete', function () {
            var tr = $(this).closest('tr');
            var data = table.row(tr).data();
            delete_record(data.id);
        }); 
        
    });

    function delete_record(id) {
        swal({
            title: "¿Desea eliminar este tipo?",
            text: "Se eliminará definitivamente del listado",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Eliminar",
            closeOnConfirm: false
        }).then(function (isConfirm) {
            if (isConfirm) {
               $.ajax({
                     type: "POST",
                     url: base_url+"TipoMedicamento/delete/"+id,
                     success: function (result) {
                        console.log(result);
                        table.ajax.reload();
                        swal("Éxito!", "Se ha eliminado correctamente", "success");
                     }
                 });
            }
        });

    }

