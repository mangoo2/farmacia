var base_url = $('#base_url').val();
$(document).ready(function() {	
	$("#cefp").hide();
	$('#vcliente').select2({
		width: 'resolve',
		minimumInputLength: 3,
		minimumResultsForSearch: 10,
		placeholder: 'Buscar un cliente',
	  	ajax: {
	    	url: 'Ventas/searchcli',
	    	dataType: "json",
	    	data: function (params) {
	      	var query = {
	        	search: params.term,
	        	type: 'public'
	      	}
	      	return query;
	    },
	    processResults: function(data){
	    	var clientes=data;
	    	var itemscli = [];
	    	data.forEach(function(element) {
                itemscli.push({
                    id: element.ClientesId,
                    text: element.Nom
                });
            });
            return {
                results: itemscli
            };	    	
	    },  
	  }
	});

	$('#vproducto').select2({
		width: 'resolve',
		minimumInputLength: 3,
		minimumResultsForSearch: 10,
		placeholder: 'Buscar un Producto',
	  	ajax: {
	    	url: 'Ventas/searchproducto',
	    	dataType: "json",
	    	data: function (params) {
	      	var query = {
	        	search: params.term,
	        	type: 'public'
	      	}
	      	return query;
	    },
	    processResults: function(data){
	    	var clientes=data;
	    	var itemscli = [];
	    	data.forEach(function(element) {
                itemscli.push({
                    id: element.productoid,
                    text: element.codigo+' / '+element.nombre
                });
            });
            return {
                results: itemscli
            };	    	
	    },  
	  }
	}).on('select2:select', function (e) {
	    //var data = e.params.data;
	    //console.log(data);
	    addproducto();
	});
	$('#vproducto').select2('open').on('focus');
	$('#ingresaventa').click(function(event) {
            addventas();
            
    });
    $('#btnabrirt').click(function(){
			$.ajax({
				type:'POST',
				url:'Ventas/abrirturno',
				data:{
					cantidad: $('#cantidadt').val(),
					nombre: $('#nombredelturno').val()
				},
				async:false,
				success:function(data){
					toastr.success('Turno Abierto','Hecho!');
					$('#modalturno').modal('hide');
					
				}
			});
		
	});
	$( "#vingreso" ).keypress(function(e) {
	  	if(e.which == 13) {
       		addventas();
    	}
	});
	$( "#vingresot" ).keypress(function(e) {
	  	if(e.which == 13) {
       		addventas();
    	}
	});
});
function addproducto(){
	if ($('#vcantidad').val()>0) {
		$.ajax({
	        type:'POST',
	        url: 'Ventas/addproducto',
	        data: {
	            cant: $('#vcantidad').val(),
	            prod: $('#vproducto').val()
	            },
	            async: false,
	            statusCode:{
	                404: function(data){
	                    toastr.error('Error!', 'No Se encuentra el archivo');
	                },
	                500: function(){
	                    toastr.error('Error', '500');
	                }
	            },
	            success:function(data){
	            	//console.log(data);
	                $('#class_productos').html(data);
	            }
	        });
		$('#vcantidad').val(1);
		$('#vproducto').html('');
	    $("#vproducto").val(0).change();
	    $('#vproducto').select2('open').on('focus');
	}
    calculartotal();
}

function mandaPrecio(id,precio){
    //var idr=$('.prec_'+id).data('rowprecio');
    //console.log("id en mandar precio: "+id);
    //console.log("precio: "+precio);
    $.ajax({
        type:'POST',
        url: base_url+'Ventas/precio_prod',
        data: {
            id: id,
            precio: precio
        },
        success:function(data){
            //console.log(data);
            //console.log("ok");
        }
    });
}

function recalcula(id){
    var precio = $(".prec_"+id+"").val();
    var cant = $(".cant_"+id+"").val();
    var total = cant * precio;
    //console.log("id: "+id);
    //console.log("precio: "+precio);

    $(".prec_"+id+"").val('');
    $(".prec_"+id+"").val(precio);
    $(".vstotal_"+id+"").val(total);
    //$("#band_"+id+"").val(total);
    //band=1;
    calculartotal();
    setTimeout(function(){ 
        mandaPrecio(id,$(".prec_"+id+"").val());
    }, 1000);
    
}


function calculartotal(){
	var addtp = 0;
	$(".vstotal").each(function() {
        var vstotal = $(this).val();
        addtp += Number(vstotal);
    });
    //var descuento1=100/$('#mdescuento').val();
    $('#vsbtotal').val(addtp);
    var descuento=addtp*$('#mdescuento').val();
    $('#cantdescuento').val(descuento);
    var total=parseFloat(addtp)-parseFloat(descuento);
    $('#vtotal').val(Number(total).toFixed(2));
    ingreso();
}

function realizec(){
	valor=$('input:radio[name=moneda]:checked').val();
	if(valor=="D"){
		$("#cefp").show();
	}else{
		$("#cefp").hide();
	}
}

function ingreso(){
	valor=$('input:radio[name=moneda]:checked').val();
	moneda=1;
	if(valor=="D"){
		moneda=GetDtaMoneda();
	}
	var ingresoe= $('#vingreso').val()*moneda;
	$('#vingresoP').val(ingresoe);
	var ingresot= $('#vingresot').val();
	var ingreso=parseFloat(ingresoe)+parseFloat(ingresot);
	var totals=$('#vtotal').val();

	if(ingreso>0)
		var cambio = parseFloat(ingreso)-parseFloat(totals);
	//if (cambio<0) {
	//	cambio=0;
	//}
	$('#vcambio').val(cambio);
}

function GetCambio(){
	ingreso();
	valor=$('input:radio[name=Cmon]:checked').val();
	moneda=1;
	if(valor=="D"){
		moneda=GetDtaMoneda();
	}
	cambio=$('#vcambio').val()/moneda;
	$('#vcambio').val(cambio.toFixed(2));
}

function limpiar(){
	$('#class_productos').html('');
	$.ajax({
	        type:'POST',
	        url: 'Ventas/productoclear',
	        async: false,
	        statusCode:{
	            404: function(data){
	            	toastr.error('Error!', 'No Se encuentra el archivo');
	        	},
	        	500: function(){
	                toastr.error('Error', '500');
	            }
	        },
	        success:function(data){
	            	
	        }
	});
}
function deletepro(id){
	
	$.ajax({
        type:'POST',
        url: 'Ventas/deleteproducto',
        data: {
            idd: id
            },
            async: false,
            statusCode:{
                404: function(data){
                    toastr.error('Error!', 'No Se encuentra el archivo');
                },
                500: function(){
                    toastr.error('Error', '500');
                }
            },
            success:function(data){
            	$('.producto_'+id).remove();
            }
        });
}
function addventas(){
	var pagado = 1;
	var vtotal = $('#vtotal').val();
	valor=$('input:radio[name=moneda]:checked').val();
	if(valor=="D"){
		efectivo=$("#vingresoP").val();
	}else{
		efectivo=$("#vingreso").val();
	}
	var vingresot =$('#vingresot').val()==''?0:$('#vingresot').val();
	vingresot=parseFloat(vingresot);
	vtotal=parseFloat(vtotal);
	if($('#mpago').val()==4){
		pagado=0;
	}
	pago=efectivo+vingresot;
	if ($('#vcambio').val()>=0 && (pago>=vtotal || $('#mpago').val()==4)) {
		efectivo=vtotal-vingresot;
        $.ajax({
            type:'POST',
            url: 'Ventas/ingresarventa',
            data: {
            	uss: $('#ssessius').val(),
                cli: $('#vcliente').val(),
                mpago: $('#mpago').val(),
                desc: $('#mdescuento').val(),
                descu: $('#cantdescuento').val(),
                sbtotal:$('#vsbtotal').val(),
                total: vtotal,
                efectivo: efectivo,
                tarjeta: vingresot,
                pagado: pagado
            },
            async: false,
            statusCode:{
                404: function(data){
                    toastr.error('Error!', 'No Se encuentra el archivo');
                },
                500: function(){
                    toastr.error('Error', '500');
                }
            },
            success:function(data){
            	var idventa=data;
            	var DATA  = [];
    			var TABLA   = $("#productosv tbody > tr");
    				TABLA.each(function(){         
		                item = {};
		                item ["idventa"] = idventa;
		                item ["producto"]   = $(this).find("input[id*='vsproid']").val();
		                item ["cantidad"]  = $(this).find("input[id*='vscanti']").val();
		                item ["precio"]  = $(this).find("input[id*='vsprecio']").val();
		                DATA.push(item);
		            });
    				INFO  = new FormData();
    				aInfo   = JSON.stringify(DATA);
    				INFO.append('data', aInfo);
		            $.ajax({
		                data: INFO,
		                type: 'POST',
		                url : 'Ventas/ingresarventapro',
		                processData: false, 
		                contentType: false,
		                async: false,
	                    statusCode:{
	                        404: function(data){
	                            toastr.error('Error!', 'No Se encuentra el archivo');
	                        },
	                        500: function(){
	                            toastr.error('Error', '500');
	                        }
	                    },
		                success: function(data){
		                }
		            });
		            checkprint = document.getElementById("checkimprimir").checked;
					if (checkprint==true) {
						$("#iframeri").modal();
						//$('#iframereporte').html('<iframe src="Visorpdf?filex=Ticket&iden=id&id='+idventa+'"></iframe>');	
						$('#iframereporte').html('<iframe src="'+base_url+'Ticket?id='+idventa+'"></iframe>');		
					}else{
						toastr.success( 'Venta Realizada','Hecho!');
					}
					limpiar();
				    $("#vcliente").val(1).change();
				    $('#vtotal').val(0);
				    $('#vcambio').val(0);
				    $('#vingreso').val(0);
				    $('#vingresoP').val(0);
				    $("#vingresot").val(0);
            }
        });
            }else{
            	toastr.error('No se puede realizar la venta debido a que no ha ingresado el saldo para liquidar la venta','Error!');
            }
}

function GetDtaMoneda(){
	var valor;
	$.ajax({
        type:'POST',
        url: 'Ventas/GetDolarValue',
        async: false,
        success:function(data){
        	valor=data;
        }
	});
	return valor;
}
