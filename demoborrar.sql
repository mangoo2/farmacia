-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 25-02-2021 a las 01:40:19
-- Versión del servidor: 10.1.13-MariaDB
-- Versión de PHP: 5.5.35

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `demoborrar`
--

DELIMITER $$
--
-- Procedimientos
--
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_ADD_PERSONAL` (IN `_NOM` VARCHAR(120), IN `_SEX` INT(1))  NO SQL
INSERT INTO personal (nombre,sexo)VALUES (_NOM,_SEX)$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_DEL_PERSONAL` (IN `_ID` INT)  NO SQL
UPDATE personal SET estatus=0 where personalId=_ID$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_GET_ALL_ESTADOS` ()  NO SQL
SELECT * FROM Estado$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_GET_ALL_PERFILES` ()  NO SQL
SELECT *  FROM Perfiles$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_GET_ALL_PERSONAL` ()  NO SQL
SELECT per.personalId,per.nombre,per.apellidos,usu.Usuario 
FROM personal as per 
LEFT JOIN usuarios as usu on usu.personalId=per.personalId
WHERE per.tipo=1 AND per.estatus=1$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_GET_MENUS` (IN `PERFIL` INT)  NO SQL
SELECT distinct men.MenuId,men.Nombre,men.Icon from menu as men, menu_sub as mens, Perfiles_detalles as perfd where men.MenuId=mens.MenuId and perfd.MenusubId=mens.MenusubId and perfd.PerfilId=PERFIL$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_GET_PERSONAL` (IN `_ID` INT)  NO SQL
SELECT *  FROM personal where personalId=_ID$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_GET_SESSION` (IN `USUA` VARCHAR(10))  NO SQL
SELECT usu.UsuarioID,per.personalId,per.nombre, usu.perfilId, usu.contrasena FROM usuarios as usu,personal as per where per.personalId = usu.personalId and usu.Usuario = USUA$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_GET_USUARIOS` ()  NO SQL
SELECT usu.UsuarioID,usu.Usuario,per.nombre as perfil FROM usuarios as usu, Perfiles as per WHERE usu.perfilId=per.perfilId$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `categoria`
--

CREATE TABLE `categoria` (
  `categoriaId` int(11) NOT NULL,
  `categoria` varchar(200) NOT NULL,
  `activo` int(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `categoria`
--

INSERT INTO `categoria` (`categoriaId`, `categoria`, `activo`) VALUES
(1, 'Muebles', 1),
(2, 'ssss', 0),
(3, 's', 0),
(4, 'ARTICULOS DE BAÑO', 1),
(5, 'LAMPARAS', 1),
(6, 'ARTESANIAS', 1),
(7, 'CHAROLAS', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `clasificacion`
--

CREATE TABLE `clasificacion` (
  `id` int(11) NOT NULL,
  `nombre` varchar(100) NOT NULL,
  `activo` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `clasificacion`
--

INSERT INTO `clasificacion` (`id`, `nombre`, `activo`) VALUES
(1, 'drogas fuertes', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `clientes`
--

CREATE TABLE `clientes` (
  `ClientesId` int(11) NOT NULL,
  `Nom` varchar(50) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `Calle` varchar(45) DEFAULT NULL,
  `noExterior` varchar(45) DEFAULT NULL,
  `Colonia` varchar(45) DEFAULT NULL,
  `Localidad` varchar(45) DEFAULT NULL,
  `Municipio` varchar(45) DEFAULT NULL,
  `Estado` varchar(45) DEFAULT NULL,
  `Pais` varchar(45) DEFAULT NULL,
  `CodigoPostal` varchar(45) DEFAULT NULL,
  `Correo` varchar(100) NOT NULL,
  `noInterior` varchar(45) NOT NULL,
  `nombrec` varchar(100) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `correoc` varchar(45) NOT NULL,
  `telefonoc` varchar(30) NOT NULL,
  `extencionc` varchar(20) NOT NULL,
  `nextelc` varchar(30) NOT NULL,
  `descripcionc` text NOT NULL,
  `activo` int(1) NOT NULL DEFAULT '1' COMMENT '1 activo 0 eliminado'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `clientes`
--

INSERT INTO `clientes` (`ClientesId`, `Nom`, `Calle`, `noExterior`, `Colonia`, `Localidad`, `Municipio`, `Estado`, `Pais`, `CodigoPostal`, `Correo`, `noInterior`, `nombrec`, `correoc`, `telefonoc`, `extencionc`, `nextelc`, `descripcionc`, `activo`) VALUES
(1, 'PUBLICO EN GENERAL', '', '', '', '', '', '', 'Mexico', '', '', '', '', '', '', '', '', '', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `compras`
--

CREATE TABLE `compras` (
  `id_compra` int(11) NOT NULL,
  `id_proveedor` int(11) NOT NULL,
  `monto_total` double NOT NULL,
  `reg` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `compra_detalle`
--

CREATE TABLE `compra_detalle` (
  `id_detalle_compra` int(11) NOT NULL,
  `id_compra` int(11) NOT NULL,
  `id_producto` int(11) NOT NULL,
  `cantidad` float NOT NULL,
  `precio_compra` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `estado`
--

CREATE TABLE `estado` (
  `EstadoId` int(11) NOT NULL,
  `Nombre` varchar(50) DEFAULT NULL,
  `Alias` varchar(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `estado`
--

INSERT INTO `estado` (`EstadoId`, `Nombre`, `Alias`) VALUES
(1, 'AGUASCALIENTES', 'AS'),
(2, 'BAJA CALIFORNIA', 'BC'),
(3, 'BAJA CALIFORNIA SUR', 'BS'),
(4, 'CAMPECHE', 'CC'),
(5, 'COAHUILA', 'CL'),
(6, 'COLIMA', 'CM'),
(7, 'CHIAPAS', 'CS'),
(8, 'CHIHUAHUA', 'CH'),
(9, 'DISTRITO FEDERAL', 'DF'),
(10, 'DURANGO', 'DG'),
(11, 'GUANAJUATO', 'GT'),
(12, 'GUERRERO', 'GR'),
(13, 'HIDALGO', 'HG'),
(14, 'JALISCO', 'JC'),
(15, 'ESTADO DE MEXICO', 'MC'),
(16, 'MICHOACAN', 'MN'),
(17, 'MORELOS', ''),
(18, 'NAYARIT', 'NT'),
(19, 'NUEVO LEON', 'NL'),
(20, 'OAXACA', 'OC'),
(21, 'PUEBLA', 'PL'),
(22, 'QUERETARO', 'QT'),
(23, 'QUINTANA ROO', 'QR'),
(24, 'SAN LUIS POTOSI', 'SP'),
(25, 'SINALOA', 'SL'),
(26, 'SONORA', 'SR'),
(27, 'TABASCO', 'TC'),
(28, 'TAMAULIPAS', 'TS'),
(29, 'TLAXCALA', 'TL'),
(30, 'VERACRUZ', 'VZ'),
(31, 'YUCATAN', 'YN'),
(32, 'ZACATECAS', 'ZS');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `menu`
--

CREATE TABLE `menu` (
  `MenuId` int(11) NOT NULL,
  `Nombre` varchar(45) DEFAULT NULL,
  `Icon` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `menu`
--

INSERT INTO `menu` (`MenuId`, `Nombre`, `Icon`) VALUES
(1, 'Catálogos', 'fa fa-book'),
(2, 'Operaciones', 'fa fa-folder-open'),
(3, 'Configuración\n', 'fa fa fa-cogs');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `menu_sub`
--

CREATE TABLE `menu_sub` (
  `MenusubId` int(11) NOT NULL,
  `MenuId` int(11) NOT NULL,
  `Nombre` varchar(50) DEFAULT NULL,
  `Pagina` varchar(120) DEFAULT NULL,
  `Icon` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `menu_sub`
--

INSERT INTO `menu_sub` (`MenusubId`, `MenuId`, `Nombre`, `Pagina`, `Icon`) VALUES
(1, 1, 'Productos', 'Productos', 'fa fa-barcode'),
(2, 3, 'Categoria', 'Categoria', 'fa fa-cogs'),
(3, 1, 'Personal', 'Personal', 'fa fa-group'),
(4, 2, 'Ventas', 'Ventas', 'fa fa-shopping-cart'),
(5, 2, 'Compras', 'Compras', 'fa fa-cogs'),
(6, 1, 'Clientes', 'Clientes', 'fa fa-user'),
(7, 1, 'Proveedores', 'Proveedores', 'fa fa-truck'),
(8, 2, 'Corte de caja', 'Corte_caja', 'fa fa-cogs'),
(9, 2, 'Lista de ventas', 'ListaVentas', 'fa fa-cogs'),
(10, 2, 'Turno', 'Turno', 'fa fa-cogs'),
(11, 2, 'Lista de turnos', 'ListaTurnos', 'fa fa-cogs'),
(12, 2, 'Lista de compras', 'Listacompras', 'fa fa-shopping-cart'),
(13, 3, 'Config. de ticket', 'Config_ticket', 'fa fa-cogs'),
(14, 3, 'Clasificaciones', 'Clasificaciones', 'fa fa-cogs');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `notas`
--

CREATE TABLE `notas` (
  `id_nota` int(1) NOT NULL,
  `mensaje` text NOT NULL,
  `usuario` varchar(120) NOT NULL,
  `reg` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `notas`
--

INSERT INTO `notas` (`id_nota`, `mensaje`, `usuario`, `reg`) VALUES
(1, '<p>notass</p>\n', 'Administrador', '2018-05-10 19:01:36');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `perfiles`
--

CREATE TABLE `perfiles` (
  `perfilId` int(11) NOT NULL,
  `nombre` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `perfiles`
--

INSERT INTO `perfiles` (`perfilId`, `nombre`) VALUES
(1, 'Administrador'),
(2, 'Personal'),
(3, 'Residentes');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `perfiles_detalles`
--

CREATE TABLE `perfiles_detalles` (
  `Perfil_detalleId` int(11) NOT NULL,
  `perfilId` int(11) NOT NULL,
  `MenusubId` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `perfiles_detalles`
--

INSERT INTO `perfiles_detalles` (`Perfil_detalleId`, `perfilId`, `MenusubId`) VALUES
(1, 1, 1),
(2, 1, 2),
(3, 1, 3),
(4, 1, 4),
(5, 1, 5),
(6, 2, 4),
(7, 2, 5),
(8, 1, 14);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `personal`
--

CREATE TABLE `personal` (
  `personalId` int(11) NOT NULL,
  `nombre` varchar(300) NOT NULL,
  `apellidos` varchar(300) NOT NULL,
  `fechanacimiento` date NOT NULL,
  `sexo` int(1) NOT NULL,
  `domicilio` varchar(500) NOT NULL,
  `ciudad` varchar(120) NOT NULL,
  `estado` int(11) NOT NULL,
  `codigopostal` int(5) NOT NULL,
  `telefono` varchar(15) NOT NULL,
  `celular` varchar(15) NOT NULL,
  `correo` varchar(50) NOT NULL,
  `turno` int(1) NOT NULL,
  `fechaingreso` date NOT NULL,
  `fechabaja` date NOT NULL,
  `sueldo` decimal(10,2) NOT NULL,
  `tipo` int(1) NOT NULL DEFAULT '1' COMMENT '0 administrador 1 normal',
  `estatus` int(1) NOT NULL DEFAULT '1' COMMENT '1 visible 0 eliminado'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

--
-- Volcado de datos para la tabla `personal`
--

INSERT INTO `personal` (`personalId`, `nombre`, `apellidos`, `fechanacimiento`, `sexo`, `domicilio`, `ciudad`, `estado`, `codigopostal`, `telefono`, `celular`, `correo`, `turno`, `fechaingreso`, `fechabaja`, `sueldo`, `tipo`, `estatus`) VALUES
(1, 'Administrador', '', '0000-00-00', 0, '', '', 0, 0, '', '', '', 0, '0000-00-00', '0000-00-00', '0.00', 0, 1),
(2, 'gerardo', 'bautista', '2018-05-02', 1, 'conocido', 'conocido', 30, 94140, '123456789', '1234567890', 'soporte@mangoo.mx', 3, '2018-05-03', '2018-05-17', '30000.00', 1, 1),
(3, '_NOM', '', '0000-00-00', 0, '', '', 0, 0, '', '', '', 0, '0000-00-00', '0000-00-00', '0.00', 1, 0),
(4, 'Mariana gutierrez', '', '0000-00-00', 2, '', '', 0, 0, '', '', '', 0, '0000-00-00', '0000-00-00', '0.00', 1, 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `personal_menu`
--

CREATE TABLE `personal_menu` (
  `personalmenuId` int(11) NOT NULL,
  `personalId` int(11) NOT NULL,
  `MenuId` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `personal_menu`
--

INSERT INTO `personal_menu` (`personalmenuId`, `personalId`, `MenuId`) VALUES
(1, 1, 1),
(2, 1, 2),
(3, 1, 3),
(4, 1, 4),
(5, 1, 5),
(6, 1, 6),
(7, 1, 7),
(8, 1, 8),
(9, 1, 9),
(10, 1, 10),
(11, 1, 11),
(12, 1, 12),
(13, 1, 13),
(17, 2, 4),
(18, 1, 14);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `productos`
--

CREATE TABLE `productos` (
  `productoid` int(11) NOT NULL,
  `codigo` varchar(20) NOT NULL,
  `productofiscal` int(1) DEFAULT NULL COMMENT '0 no 1 si',
  `nombre` varchar(120) NOT NULL,
  `tipo` varchar(100) DEFAULT NULL,
  `concentracion` varchar(100) DEFAULT NULL,
  `piezas` int(11) DEFAULT NULL,
  `articulo` varchar(100) DEFAULT NULL,
  `clasificacion` int(11) DEFAULT NULL,
  `substancia` varchar(100) DEFAULT NULL,
  `categoria` int(11) NOT NULL,
  `stock` int(11) NOT NULL,
  `stockmin` float NOT NULL DEFAULT '0',
  `preciocompra` decimal(10,2) NOT NULL,
  `porcentaje` int(11) NOT NULL,
  `precioventa` decimal(10,2) NOT NULL,
  `mediomayoreo` decimal(10,2) NOT NULL,
  `canmediomayoreo` int(11) NOT NULL,
  `mayoreo` decimal(10,2) NOT NULL,
  `canmayoreo` int(11) NOT NULL,
  `tipopro` int(11) NOT NULL COMMENT '1.- farmacia, 2.- tienda, 3.- servicio',
  `activo` int(1) NOT NULL DEFAULT '1' COMMENT '1 actual 0 eliminado',
  `reg` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `productos`
--

INSERT INTO `productos` (`productoid`, `codigo`, `productofiscal`, `nombre`, `tipo`, `concentracion`, `piezas`, `articulo`, `clasificacion`, `substancia`, `categoria`, `stock`, `stockmin`, `preciocompra`, `porcentaje`, `precioventa`, `mediomayoreo`, `canmediomayoreo`, `mayoreo`, `canmayoreo`, `tipopro`, `activo`, `reg`) VALUES
(1, '123123', NULL, '123123', '123123', '123123', 123123, '123123', 1, '123123', 0, 133, 12, '123.00', 1, '123.00', '0.00', 0, '123.00', 119, 1, 1, '2021-02-24 17:07:16'),
(2, '123123', NULL, '123123', '123123', '123123', 123123, '123123', 1, '123123', 0, 133, 12, '123.00', 1, '123.00', '0.00', 0, '123.00', 119, 1, 1, '2021-02-24 17:08:39'),
(3, '12331', NULL, '1231', '12', '123', 123, '12312', 1, '231', 0, 123, 123, '12.00', 12, '123.00', '0.00', 0, '123.00', 123, 1, 1, '2021-02-24 17:12:35'),
(4, '123', NULL, '123', '123', '123', 123, '123', 1, '123', 0, 123, 123, '123.00', 123, '123.00', '0.00', 0, '123.00', 122, 1, 1, '2021-02-24 17:13:55'),
(5, '123', NULL, '12313', '123', '123', 123, '123', 1, '123', 0, 123, 123, '123.00', 1, '123.00', '0.00', 0, '2.00', 31, 1, 1, '2021-02-24 17:21:15');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `producto_proveedor`
--

CREATE TABLE `producto_proveedor` (
  `id` bigint(20) NOT NULL,
  `id_producto` int(11) NOT NULL,
  `id_proveedor` int(11) NOT NULL,
  `precio` float NOT NULL,
  `activo` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `producto_proveedor`
--

INSERT INTO `producto_proveedor` (`id`, `id_producto`, `id_proveedor`, `precio`, `activo`) VALUES
(1, 5, 23, 231, 1),
(2, 5, 23, 123, 1),
(3, 5, 23, 232, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `producto_similar`
--

CREATE TABLE `producto_similar` (
  `id` bigint(20) NOT NULL,
  `id_producto` int(11) NOT NULL,
  `similar` varchar(100) NOT NULL,
  `activo` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `producto_similar`
--

INSERT INTO `producto_similar` (`id`, `id_producto`, `similar`, `activo`) VALUES
(1, 5, '1231', 1),
(2, 5, '312', 1),
(3, 5, '312', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `proveedores`
--

CREATE TABLE `proveedores` (
  `id_proveedor` int(11) NOT NULL,
  `razon_social` varchar(50) NOT NULL,
  `domicilio` varchar(30) CHARACTER SET utf8 NOT NULL COMMENT 'calle',
  `ciudad` varchar(50) CHARACTER SET utf8 NOT NULL,
  `cp` varchar(8) CHARACTER SET utf8 NOT NULL,
  `id_estado` int(11) NOT NULL,
  `telefono_local` varchar(10) NOT NULL,
  `telefono_celular` varchar(10) NOT NULL,
  `contacto` varchar(100) NOT NULL,
  `email_contacto` varchar(60) NOT NULL,
  `rfc` varchar(13) NOT NULL,
  `fax` varchar(20) NOT NULL,
  `obser` text NOT NULL,
  `activo` int(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `proveedores`
--

INSERT INTO `proveedores` (`id_proveedor`, `razon_social`, `domicilio`, `ciudad`, `cp`, `id_estado`, `telefono_local`, `telefono_celular`, `contacto`, `email_contacto`, `rfc`, `fax`, `obser`, `activo`) VALUES
(23, 'GENERAL', 'AAA', 'AAA', '12345', 21, '123', '123', 'AAA', 'AAA', 'HERD890308UAA', '', '', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ticket`
--

CREATE TABLE `ticket` (
  `id_ticket` int(11) NOT NULL,
  `titulo` text CHARACTER SET utf8 NOT NULL,
  `mensaje` text CHARACTER SET utf8 NOT NULL,
  `mensaje2` text CHARACTER SET utf8 NOT NULL,
  `fuente` int(1) NOT NULL,
  `tamano` int(2) NOT NULL,
  `margensup` int(2) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `ticket`
--

INSERT INTO `ticket` (`id_ticket`, `titulo`, `mensaje`, `mensaje2`, `fuente`, `tamano`, `margensup`) VALUES
(1, 'MICELANEA', '¡GRACIAS POR SU COMPRA!', 'ESTE TICKET NO ES UNA REPRESENTACIÓN FISCAL', 2, 9, 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `turno`
--

CREATE TABLE `turno` (
  `id` int(11) NOT NULL,
  `fecha` date NOT NULL,
  `horaa` time NOT NULL,
  `fechacierre` date NOT NULL,
  `horac` time NOT NULL,
  `cantidad` float NOT NULL,
  `nombre` varchar(50) NOT NULL,
  `status` varchar(50) NOT NULL,
  `user` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `turno`
--

INSERT INTO `turno` (`id`, `fecha`, `horaa`, `fechacierre`, `horac`, `cantidad`, `nombre`, `status`, `user`) VALUES
(1, '2016-10-14', '00:37:05', '0000-00-00', '00:37:45', 100, 'x', 'cerrado', 'user');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

CREATE TABLE `usuarios` (
  `UsuarioID` int(11) NOT NULL,
  `perfilId` int(11) NOT NULL,
  `personalId` int(11) NOT NULL,
  `Usuario` varchar(45) DEFAULT NULL,
  `contrasena` varchar(80) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `usuarios`
--

INSERT INTO `usuarios` (`UsuarioID`, `perfilId`, `personalId`, `Usuario`, `contrasena`) VALUES
(1, 1, 1, 'admin', '$2y$10$.yBpfsTTEmhYhECki.qy3eL45XaLo3MinuIPgiPsjmXBYItZXHUga'),
(2, 2, 2, 'gerardo', '$2y$10$XE359fjs0SS/bB.L8ogiwefN8we7nm5ZfCDEGqC6gNNZy/06MTPlS');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ventas`
--

CREATE TABLE `ventas` (
  `id_venta` int(11) NOT NULL,
  `id_personal` int(11) NOT NULL,
  `id_cliente` int(11) NOT NULL,
  `metodo` int(1) NOT NULL COMMENT '1 efectivo, 2 credito, 3 debito',
  `subtotal` double NOT NULL,
  `descuento` double NOT NULL COMMENT '0 % 5 % 7%',
  `descuentocant` double NOT NULL,
  `monto_total` double NOT NULL,
  `pagotarjeta` float NOT NULL,
  `efectivo` float NOT NULL,
  `cancelado` int(11) NOT NULL,
  `hcancelacion` date NOT NULL,
  `reg` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `venta_detalle`
--

CREATE TABLE `venta_detalle` (
  `id_detalle_venta` int(11) NOT NULL,
  `id_venta` int(11) NOT NULL,
  `id_producto` int(11) NOT NULL,
  `cantidad` float NOT NULL,
  `precio` decimal(5,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `categoria`
--
ALTER TABLE `categoria`
  ADD PRIMARY KEY (`categoriaId`);

--
-- Indices de la tabla `clasificacion`
--
ALTER TABLE `clasificacion`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `clientes`
--
ALTER TABLE `clientes`
  ADD PRIMARY KEY (`ClientesId`);

--
-- Indices de la tabla `compras`
--
ALTER TABLE `compras`
  ADD PRIMARY KEY (`id_compra`),
  ADD KEY `constraint_fk_04` (`id_proveedor`);

--
-- Indices de la tabla `compra_detalle`
--
ALTER TABLE `compra_detalle`
  ADD PRIMARY KEY (`id_detalle_compra`),
  ADD KEY `constraint_fk_08` (`id_compra`),
  ADD KEY `constraint_fk_09` (`id_producto`);

--
-- Indices de la tabla `estado`
--
ALTER TABLE `estado`
  ADD PRIMARY KEY (`EstadoId`);

--
-- Indices de la tabla `menu`
--
ALTER TABLE `menu`
  ADD PRIMARY KEY (`MenuId`);

--
-- Indices de la tabla `menu_sub`
--
ALTER TABLE `menu_sub`
  ADD PRIMARY KEY (`MenusubId`),
  ADD KEY `fk_menu_sub_menu_idx` (`MenuId`);

--
-- Indices de la tabla `notas`
--
ALTER TABLE `notas`
  ADD PRIMARY KEY (`id_nota`);

--
-- Indices de la tabla `perfiles`
--
ALTER TABLE `perfiles`
  ADD PRIMARY KEY (`perfilId`);

--
-- Indices de la tabla `perfiles_detalles`
--
ALTER TABLE `perfiles_detalles`
  ADD PRIMARY KEY (`Perfil_detalleId`),
  ADD KEY `fk_Perfiles_detalles_Perfiles1_idx` (`perfilId`),
  ADD KEY `fk_Perfiles_detalles_menu_sub1_idx` (`MenusubId`);

--
-- Indices de la tabla `personal`
--
ALTER TABLE `personal`
  ADD PRIMARY KEY (`personalId`);

--
-- Indices de la tabla `personal_menu`
--
ALTER TABLE `personal_menu`
  ADD PRIMARY KEY (`personalmenuId`),
  ADD KEY `personal_fkpersona` (`personalId`),
  ADD KEY `personal_fkmenu` (`MenuId`);

--
-- Indices de la tabla `productos`
--
ALTER TABLE `productos`
  ADD PRIMARY KEY (`productoid`);

--
-- Indices de la tabla `producto_proveedor`
--
ALTER TABLE `producto_proveedor`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `producto_similar`
--
ALTER TABLE `producto_similar`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `proveedores`
--
ALTER TABLE `proveedores`
  ADD PRIMARY KEY (`id_proveedor`),
  ADD KEY `constraint_fk_27` (`id_estado`);

--
-- Indices de la tabla `ticket`
--
ALTER TABLE `ticket`
  ADD PRIMARY KEY (`id_ticket`);

--
-- Indices de la tabla `turno`
--
ALTER TABLE `turno`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`UsuarioID`),
  ADD KEY `fk_usuarios_Perfiles1_idx` (`perfilId`),
  ADD KEY `fk_usuarios_personal1_idx` (`personalId`);

--
-- Indices de la tabla `ventas`
--
ALTER TABLE `ventas`
  ADD PRIMARY KEY (`id_venta`),
  ADD UNIQUE KEY `id_venta_UNIQUE` (`id_venta`),
  ADD KEY `constraint_fk_30` (`id_cliente`),
  ADD KEY `constraint_fk_31` (`id_personal`);

--
-- Indices de la tabla `venta_detalle`
--
ALTER TABLE `venta_detalle`
  ADD PRIMARY KEY (`id_detalle_venta`),
  ADD UNIQUE KEY `id_detalle_venta_UNIQUE` (`id_detalle_venta`),
  ADD KEY `constraint_fk_15` (`id_producto`),
  ADD KEY `constraint_fk_16` (`id_venta`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `categoria`
--
ALTER TABLE `categoria`
  MODIFY `categoriaId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT de la tabla `clasificacion`
--
ALTER TABLE `clasificacion`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de la tabla `clientes`
--
ALTER TABLE `clientes`
  MODIFY `ClientesId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de la tabla `compras`
--
ALTER TABLE `compras`
  MODIFY `id_compra` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `compra_detalle`
--
ALTER TABLE `compra_detalle`
  MODIFY `id_detalle_compra` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `estado`
--
ALTER TABLE `estado`
  MODIFY `EstadoId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;
--
-- AUTO_INCREMENT de la tabla `menu`
--
ALTER TABLE `menu`
  MODIFY `MenuId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT de la tabla `menu_sub`
--
ALTER TABLE `menu_sub`
  MODIFY `MenusubId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT de la tabla `notas`
--
ALTER TABLE `notas`
  MODIFY `id_nota` int(1) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de la tabla `perfiles`
--
ALTER TABLE `perfiles`
  MODIFY `perfilId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT de la tabla `perfiles_detalles`
--
ALTER TABLE `perfiles_detalles`
  MODIFY `Perfil_detalleId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT de la tabla `personal`
--
ALTER TABLE `personal`
  MODIFY `personalId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT de la tabla `personal_menu`
--
ALTER TABLE `personal_menu`
  MODIFY `personalmenuId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT de la tabla `productos`
--
ALTER TABLE `productos`
  MODIFY `productoid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT de la tabla `producto_proveedor`
--
ALTER TABLE `producto_proveedor`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT de la tabla `producto_similar`
--
ALTER TABLE `producto_similar`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT de la tabla `proveedores`
--
ALTER TABLE `proveedores`
  MODIFY `id_proveedor` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;
--
-- AUTO_INCREMENT de la tabla `ticket`
--
ALTER TABLE `ticket`
  MODIFY `id_ticket` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de la tabla `turno`
--
ALTER TABLE `turno`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  MODIFY `UsuarioID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT de la tabla `ventas`
--
ALTER TABLE `ventas`
  MODIFY `id_venta` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `venta_detalle`
--
ALTER TABLE `venta_detalle`
  MODIFY `id_detalle_venta` int(11) NOT NULL AUTO_INCREMENT;
--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `compras`
--
ALTER TABLE `compras`
  ADD CONSTRAINT `fk_compra_proveedor` FOREIGN KEY (`id_proveedor`) REFERENCES `proveedores` (`id_proveedor`) ON UPDATE NO ACTION;

--
-- Filtros para la tabla `compra_detalle`
--
ALTER TABLE `compra_detalle`
  ADD CONSTRAINT `fk_compra_compra` FOREIGN KEY (`id_compra`) REFERENCES `compras` (`id_compra`),
  ADD CONSTRAINT `fk_compra_producto` FOREIGN KEY (`id_producto`) REFERENCES `productos` (`productoid`) ON UPDATE NO ACTION;

--
-- Filtros para la tabla `menu_sub`
--
ALTER TABLE `menu_sub`
  ADD CONSTRAINT `fk_menu_sub_menu` FOREIGN KEY (`MenuId`) REFERENCES `menu` (`MenuId`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `perfiles_detalles`
--
ALTER TABLE `perfiles_detalles`
  ADD CONSTRAINT `fk_Perfiles_detalles_Perfiles1` FOREIGN KEY (`perfilId`) REFERENCES `perfiles` (`perfilId`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_Perfiles_detalles_menu_sub1` FOREIGN KEY (`MenusubId`) REFERENCES `menu_sub` (`MenusubId`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `personal_menu`
--
ALTER TABLE `personal_menu`
  ADD CONSTRAINT `personal_fkmenu` FOREIGN KEY (`MenuId`) REFERENCES `menu_sub` (`MenusubId`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `personal_fkpersona` FOREIGN KEY (`personalId`) REFERENCES `personal` (`personalId`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Filtros para la tabla `usuarios`
--
ALTER TABLE `usuarios`
  ADD CONSTRAINT `fk_usuarios_Perfiles1` FOREIGN KEY (`perfilId`) REFERENCES `perfiles` (`perfilId`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `usuarios_ibfk_1` FOREIGN KEY (`personalId`) REFERENCES `personal` (`personalId`);

--
-- Filtros para la tabla `ventas`
--
ALTER TABLE `ventas`
  ADD CONSTRAINT `fk_venta_cliente` FOREIGN KEY (`id_cliente`) REFERENCES `clientes` (`ClientesId`) ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_venta_presonal` FOREIGN KEY (`id_personal`) REFERENCES `personal` (`personalId`) ON UPDATE NO ACTION;

--
-- Filtros para la tabla `venta_detalle`
--
ALTER TABLE `venta_detalle`
  ADD CONSTRAINT `fk_detalle_producto` FOREIGN KEY (`id_producto`) REFERENCES `productos` (`productoid`) ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_detalle_venta` FOREIGN KEY (`id_venta`) REFERENCES `ventas` (`id_venta`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
