<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class TipoMedicamento extends CI_Controller {

	public function __construct() {
        parent::__construct();
        $this->load->helper('url');
        $this->load->model('ModeloGeneral');
        /*if (!$this->session->userdata('logeado')){
          redirect('/Login');
	    }else{
	        $this->perfilid=$this->session->userdata('perfilid');
	        //ira el permiso del modulo
	        $this->load->model('Login_model');
	        $permiso=$this->Login_model->getviewpermiso($this->perfilid,7);// 7 es el id del submenu
	        if ($permiso==0) {
	            redirect('/Sistema');
	        }
	    }*/
    }

	public function index(){
		$this->load->view('templates/header');
        $this->load->view('templates/navbar');
		$this->load->view('tiposMed/listado');
		$this->load->view('templates/footer');
        $this->load->view('tiposMed/tipos_listajs');
	}

	public function alta($id=0){
		if($id>0){
			$data['tipos'] = $this->ModeloGeneral->get_record($id,"tipo_medica");
		}
		else{
			$data="";
		}
		$this->load->view('templates/header');
        $this->load->view('templates/navbar');
		$this->load->view('tiposMed/form',$data);
		$this->load->view('templates/footer');
        $this->load->view('tiposMed/tipos_addjs');
	}

	public function datatable_records(){
		$params = $this->input->post();
		$datas = $this->ModeloGeneral->get_recordWeAll("estatus=1","tipo_medica");
        $json_data = array("data" => $datas);
        echo json_encode($json_data);
	}

	public function submit(){
		$data=$this->input->post();
		if(!isset($data['id'])){ //insert
			$id=$this->ModeloGeneral->add_record($data,"tipo_medica");
		}
		else{ //update
			$id=$data["id"]; unset($data["id"]);
			$this->ModeloGeneral->edit_record($id,'id',$data,'tipo_medica');
		}
	}

	public function delete($id){
		$this->ModeloGeneral->edit_record($id,"id",array("estatus"=>0),"tipo_medica");
	}

}
