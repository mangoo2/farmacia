<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Productos extends CI_Controller {
    function __construct(){
        parent::__construct();
        $this->load->helper('url');
        $this->load->model('ModeloProductos');
        $this->load->model('ModeloGeneral');
        $this->load->model('ModeloVentas');
    }

	public function index(){
            $this->load->view('templates/header');
            $this->load->view('templates/navbar');
            $this->load->view('productos/productos');
            $this->load->view('templates/footer');
            $this->load->view('productos/jsproductol');
	}

    public function productosadd($id=0){
        $data["categorias"] = $this->ModeloGeneral->get_table_active("categoria");
        $data["tipomed"] = $this->ModeloGeneral->get_recordWeAll("estatus=1","tipo_medica");
        $data["provedores"] = $this->ModeloGeneral->get_table_active("proveedores");
        $data["clasificaciones"] = $this->ModeloGeneral->get_table_active("clasificacion");
        $data["prodParSim"] = $this->ModeloGeneral->get_table_active("productos");
        $data["subEstancia"] = $this->ModeloGeneral->get_recordWeAll("estatus=1","substancia");
        $data['elem']=$this->ModeloGeneral->get_recordWeAll("id_producto=".$id,"producto_similar");
        $elemens=array();
        foreach ($data['elem'] as $ey) {
            array_push($elemens,$ey->similar);
        }
        $data['charged']=$elemens;

        if ($id>0) {
            $data['productype']=$this->ModeloGeneral->SimpleGetC("productos","productoid=".$id,"tipopro");
            $personalv=$this->ModeloProductos->getproducto($id);
            foreach ($personalv->result() as $item){
                $data['label']='Editar Producto';
                $data['substancia']=$item->substancia;
                $data['id'] = $item->productoid;
                $data['tipo']=$item->tipo;
                $data['concentracion']=$item->concentracion;
                $data['piezas']=$item->piezas;
                $data['articulo']=$item->articulo;
                $data['clasificacion']=$item->clasificacion;
                $data['codigo'] = $item->codigo;
                $data['productofiscal'] = $item->productofiscal;
                $data['nombre'] = $item->nombre;
                $data['categoria'] = $item->categoria;
                $data['stock'] = $item->stock;
                $data['preciocompra'] = $item->preciocompra;
                $data['porcentaje'] = $item->porcentaje;
                $data['precioventa'] = $item->precioventa;
                $data['mediomayoreo'] = $item->mediomayoreo;
                $data['canmediomayoreo'] = $item->canmediomayoreo;
                $data['mayoreo'] = $item->mayoreo;
                $data['canmayoreo'] = $item->canmayoreo;
                $data['stockmin']=$item->stockmin;
            }
        }else{
          $data['productype'] = 0;
          $data['label']='Nuevo producto';
          $data['substancia']="";
          $data['id']=0;
          $data['tipo']="";
          $data['concentracion']="";
          $data['piezas']="";
          $data['articulo']="";
          $data['clasificacion']="";
          $data['productoid']='';
          $data['codigo']='';
          $data['productofiscal']='';
          $data['nombre']='';
          $data['descripcion']='';
          $data['categoria']='';
          $data['stock']='';
          $data['preciocompra']='';
          $data['porcentaje']=0;
          $data['precioventa']='';
          $data['mediomayoreo']='';
          $data['canmediomayoreo']='';
          $data['mayoreo']='';
          $data['canmayoreo']='';
          $data['stockmin']=0;    
        }
        $this->load->view('templates/header');
        $this->load->view('templates/navbar');
        $this->load->view('productos/productoadd',$data);
        $this->load->view('templates/footer');
        $this->load->view('productos/jsproducto');
    }

    public function SaveServicio(){
        $data=$this->input->post();
        $id=$data['id'];
        unset($data['id']);
        if ($id>0){
            $this->ModeloGeneral->edit_record($id,'productoid',$data,'productos');
            echo $id;
        }else{
            $id=$this->ModeloGeneral->add_record($data,'productos');
            echo $id;
        }
    }

    function productoadd(){
        $data=$this->input->post();
        $id=$data['id'];
        unset($data['id']);
        if ($id>0){
            $this->ModeloGeneral->edit_record($id,'productoid',$data,'productos');
            if (isset($data['tipo'])){
                $sub=array('id_substancia' => $data["substancia"], 'id_producto'=>$id);
                $tip=array('id_tipom' => $data["tipo"], 'id_producto'=>$id);
                $cla=array('id_clasificacion' => $data["clasificacion"], 'id_producto'=>$id);
            }
            echo $id;
        }else{
            $id=$this->ModeloGeneral->add_record($data,'productos');
            if (isset($data['tipo'])){
                $sub=array('id_substancia' => $data["substancia"], 'id_producto'=>$id);
                $tip=array('id_tipom' => $data["tipo"], 'id_producto'=>$id);
                $cla=array('id_clasificacion' => $data["clasificacion"], 'id_producto'=>$id);
            }
            echo $id;
        }
    }

    public function SaveRelProdProv(){
        $data=$this->input->post();
        $data['precio']=$this->fromLTN($data['precio']);
        $id=$data['idd'];
        unset($data['idd']);
        if ($data['id_proveedor']!="" && $data['precio']!="" ){
            if ($id>0){
                $this->ModeloGeneral->edit_record($id,'id',$data,'producto_proveedor');
                echo $id;
            }else{
                $id=$this->ModeloGeneral->add_record($data,'producto_proveedor');
                echo $id;
            }
        }else{
            echo "no inserta";
        }
    }

    public function GetRelProdProv(){
        $id=$this->input->post('id_producto');
        $data=$this->ModeloGeneral->get_recordWeAll("activo=1 AND id_producto=".$id,"producto_proveedor");
        echo json_encode($data);
    }

    public function GetSimi(){
        $id=$this->input->post('id_producto');
        $data=$this->ModeloGeneral->get_recordWeAll("activo=1 AND id_producto=".$id,"producto_similar");
        echo json_encode($data);
    }

    public function SaveRelProdSim(){
        $data=$this->input->post();
        $id=$data['id_producto'];
        unset($data['idd']);
        $this->ModeloGeneral->delete_records("id_producto=".$id,"producto_similar");
        if(isset($data["similar"])){
            $id_valida=$data["similar"];
            unset($data["similar"]);
        }
        for($i=0; $i<count($id_valida); $i++){
            $info = array('id_producto'=>$id, "similar"=>$id_valida[$i]);
            $this->ModeloGeneral->add_record($info,'producto_similar');
        }

    }

    public function DeletePPF(){
        $id=$this->input->post('id');
        $data=$this->ModeloGeneral->LogicDelete("producto_proveedor",$id);
    }

    public function DeleteSimi(){
        $id=$this->input->post('id');
        $data=$this->ModeloGeneral->LogicDelete("producto_similar",$id);
    }

    public function fromLTN($info){
        $r1=str_replace(',','',$info);
        $result=str_replace('$','',$r1);
        return $result;
    }


    function imgpro(){
        $idpro = $this->input->post('idpro');
        

        $upload_folder ='public/img/productos';
        $nombre_archivo = $_FILES['img']['name'];
        $tipo_archivo = $_FILES['img']['type'];
        $tamano_archivo = $_FILES['img']['size'];
        $tmp_archivo = $_FILES['img']['tmp_name'];
        //$archivador = $upload_folder . '/' . $nombre_archivo;
        $fecha=date('ymd-His');
        $tipo_archivo1 = explode('/', $tipo_archivo); 
        $tipoactivo=$tipo_archivo1[1];
        if ($tipoactivo!='png') {
            $tipo_archivo1 = explode('+', $tipoactivo); 
            $tipoactivo=$tipo_archivo1[0];
        }
        $archivador = $upload_folder . '/'.$fecha.'pro.'.$tipoactivo;
        if (!move_uploaded_file($tmp_archivo, $archivador)) {
            $return = Array('ok' => FALSE, 'msg' => 'Ocurrió un error al subir el archivo. No pudo guardarse.', 'status' => 'error');
        }else{
            $resimg=$this->ModeloProductos->imgpro($archivador,$idpro);
            $return = Array('ok'=>TRUE,'img'=>$tipoactivo);
        }
        echo json_encode($return);
    }

    public function deleteproductos(){
        $id = $this->input->post('id');
        $this->ModeloProductos->productosdelete($id); 
    }

    public function FullSelec(){
        $type=$this->input->post('type');
        $table="";
        $where="estatus=1";
        switch ($type){
            case '1':
            $table="tipo_medica";
            $data=$this->ModeloGeneral->getWhereRes($table,$where);
            break;
            case '2':
            $table="substancia";
            $data=$this->ModeloGeneral->getWhereRes($table,$where);
            break;
            case '3':
            $table="clasificacion";
            $where="activo=1";
            $data=$this->ModeloGeneral->getWhereRes($table,$where);
            break;
            case '4':
            $table="categoria";
            $where="activo=1";
            $data=$this->ModeloGeneral->getWhereResCat($table,$where);
            break;
            default:
            break;
        }
        
        echo json_encode($data);

    }

    public function DelCatEle(){
        $type=$this->input->post('type');
        $ide=$this->input->post('ide');
        $table="";
        $setd=array('estatus'=>0);
        $where="estatus=1";
        switch ($type){
            case '1':
            $table="tipo_medica";
            break;
            case '2':
            $table="substancia";
            break;
            case '3':
            $table="clasificacion";
            $setd=array('activo'=>0);
            break;
            default:
            break;
        }
        $data=$this->ModeloGeneral->edit_record($ide,"id",$setd,$table);
    }

    public function AddData(){
        $type=$this->input->post('type');
        $valor=$this->input->post('select');
        $table="";
        $data=array('nombre'=>$valor);
        switch ($type){
            case '1':
                $table="tipo_medica";
            break;
            case '2':
                $table="substancia";
            break;
            case '3':
                $table="clasificacion";
            break;
            case '4':
                $table="categoria";
                $data=array('categoria'=>$valor);
            break;
        }
        $info=$this->ModeloGeneral->add_record($data,$table);
    }

    public function GetDataCatalogo(){
        $id=$this->input->post('id');
        $type=$this->input->post('type');
        switch ($type) {
            case '1':
                $table="tipo_medica";
                $where="estatus=1";
                $data=$this->ModeloGeneral->get_recordWeAll($where,$table);
            break;
            case '2':
                $table="substancia";
                $where="estatus=1";
                $data=$this->ModeloGeneral->get_recordWeAll($where,$table);
            break;
            case '3':
                $table="clasificacion";
                $where="activo=1";
                $data=$this->ModeloGeneral->get_recordWeAll($where,$table);
            break;
            case '4':
                $table="categoria";
                $where="ta.activo=1";
                $data=$this->ModeloGeneral->get_recordWeAllCat($where,$table);
            break;
        }
        $json_data = array("data" => $data); 
        echo json_encode($json_data);
    }
    
    
    public function getlistproductos() {
        $params = $this->input->post();
        $getdata = $this->ModeloProductos->getlistproductos($params);
        $info=$getdata->result();
        $totaldata= count($info); 
        $json_data = array(
            "draw"            => intval( $params['draw'] ),   
            "recordsTotal"    => intval($totaldata),  
            "recordsFiltered" => intval($totaldata),
            "data"            => $info,
            "query"           => $this->db->last_query()   
        );
        echo json_encode($json_data);
    }

    public function getlistproductosT(){
        $params = $this->input->post();
        $getdata = $this->ModeloProductos->getlistproductosT($params);
        $info=$getdata->result();
        $totaldata= count($info); 
        $json_data = array(
            "draw"            => intval( $params['draw'] ),   
            "recordsTotal"    => intval($totaldata),  
            "recordsFiltered" => intval($totaldata),
            "data"            => $info,
            "query"           => $this->db->last_query()   
        );
        echo json_encode($json_data);
    }

    public function getlistproductosS(){
        $params = $this->input->post();
        $getdata = $this->ModeloProductos->getlistproductosS($params);
        $info=$getdata->result();
        $totaldata= count($info); 
        $json_data = array(
            "draw"            => intval( $params['draw'] ),   
            "recordsTotal"    => intval($totaldata),  
            "recordsFiltered" => intval($totaldata),
            "data"            => $info,
            "query"           => $this->db->last_query()   
        );
        echo json_encode($json_data);
    }

    public function getlistproductosE(){
        $params = $this->input->post();
        $getdata = $this->ModeloProductos->getlistproductosE($params);
        $info=$getdata->result();
        $totaldata= count($info); 
        $json_data = array(
            "draw"            => intval( $params['draw'] ),   
            "recordsTotal"    => intval($totaldata),  
            "recordsFiltered" => intval($totaldata),
            "data"            => $info,
            "query"           => $this->db->last_query()   
        );
        echo json_encode($json_data);
    }

    public function GetDataVentasC(){
        $where="pagado=0 AND metodo=4";
        $data=$this->ModeloVentas->GetDataCredito();
        $json_data = array("data" => $data); 
        echo json_encode($json_data);
    }


       
    
}
