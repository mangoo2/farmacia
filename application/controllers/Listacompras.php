<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Listacompras extends CI_Controller {
    function __construct(){
        parent::__construct();
        $this->load->helper('url');
        $this->load->model('ModeloVentas');
        $this->load->model('ModeloGeneral');
    }
	public function index(){
            $pages=10; //Número de registros mostrados por páginas
            $this->load->library('pagination'); //Cargamos la librería de paginación
            $config['base_url'] = base_url().'Clientes/view/'; // parametro base de la aplicación, si tenemos un .htaccess nos evitamos el index.php
            $config['total_rows'] = $this->ModeloVentas->filaslcompras();//calcula el número de filas
            $config['per_page'] = $pages; //Número de registros mostrados por páginas  
            $config['num_links'] = 20; //Número de links mostrados en la paginación
            $config['first_link'] = 'Primera';//primer link
            $config['last_link'] = 'Última';//último link
            $config["uri_segment"] = 3;//el segmento de la paginación
            $config['next_link'] = 'Siguiente';//siguiente link
            $config['prev_link'] = 'Anterior';//anterior link
            $this->pagination->initialize($config); //inicializamos la paginación 
            $pagex = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
            $data["lcompras"] = $this->ModeloVentas->total_paginadoslcompras($pagex,$config['per_page']);
            
            $this->load->view('templates/header');
            $this->load->view('templates/navbar');
            //$this->load->view('Personal/Personal',$data);
            $this->load->view('compras/lcompras',$data);
            $this->load->view('templates/footer');
            $this->load->view('compras/jslcompras');
	}
    function consultar(){
        $inicio = $this->input->post('fechain');
        $fin = $this->input->post('fechafin');
        $clcompras = $this->ModeloVentas->lcomprasconsultar($inicio,$fin);
        foreach ($clcompras->result() as $item) {
            echo '<tr id="trcli_'.$item->id_detalle_compra.'">
                          <td>'.$item->id_detalle_compra.'</td>
                          <td>'.$item->reg.'</td>
                          <td>'.$item->producto.'</td>
                          <td>'.$item->razon_social.'</td>
                          <td>'.$item->cantidad.'</td>
                          <td>'.$item->precio_compra.'</td>
                        </tr>';
        }
    }

    public function DeleteC(){
        $id=$this->input->post('id');
        $data=array('activo' => 0);
        $this->ModeloGeneral->edit_record($id,"id_detalle_compra",$data,"compra_detalle");
        $where="id_detalle_compra=".$id;
        $info=$this->ModeloGeneral->get_recordW($where,"compra_detalle");
        $idp=$info->id_producto;
        $can=$info->cantidad;
        $sta=$this->ModeloGeneral->SimpleGetC("productos","productoid=".$idp,"stock");
        $nstok=$sta-$can;
        $this->ModeloGeneral->edit_record($idp,"productoid",array('stock' => $nstok),"productos");
    }
}
