<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Corte_caja extends CI_Controller {
    function __construct(){
        parent::__construct();
        $this->load->helper('url');
        //$this->load->model('Personal/ModeloPersonal');
        //$this->load->model('Usuarios/ModeloUsuarios');
        $this->load->model('ModeloVentas');
        $this->load->model('ModeloGeneral');
    }
	public function index(){
            //$data['personal']=$this->ModeloPersonal->getpersonal();
            $data["personal"] = $this->ModeloGeneral->getWhereRes("personal",array("estatus"=>1));
            //carga de vistas
            $this->load->view('templates/header');
            $this->load->view('templates/navbar');
            //$this->load->view('Personal/Personal',$data);

            $this->load->view('corte/corte',$data);
            $this->load->view('templates/footer');
            $this->load->view('corte/jscorte');

	}
    function corte(){
        $inicio = $this->input->post('fecha1');
        $fin = $this->input->post('fecha2');
        $personal = $this->input->post('personal');
        $resultadoc=$this->ModeloVentas->corte($inicio,$fin,$personal);
        $resultadocs=$this->ModeloVentas->cortesum($inicio,$fin,$personal);
        
        $resultadovc=$this->ModeloVentas->corteCreditos($inicio,$fin,$personal); //corte de caja de ventas a credito pagadas
        $resultadocsvc=$this->ModeloVentas->cortesumVC($inicio,$fin,$personal); //suma de ventas a credito pagadas
        $productomayor=$this->ModeloVentas->cortevmm($inicio,$fin,$personal); //top 10 de mayor vendidos

        $resultefec=$this->ModeloVentas->corteEfectivo($inicio,$fin,$personal);
        $resultarj=$this->ModeloVentas->corteTarjeta($inicio,$fin,$personal);
        $efectivo=0;
        $tarjetas=0;
        foreach ($resultefec->result() as $fila) { 
            $efectivo = $efectivo+$fila->totalvd;
        }
        foreach ($resultarj->result() as $fila) { 
            $tarjetas=$tarjetas+$fila->pagotarjeta;
        }

        $table="<table class='table table-striped table-bordered table-hover' id='sample_1'>
                    <thead>
                        <tr>
                            <th>No. venta</th>
                            <th>Cajero</th>
                            <th>Cliente</th>
                            <th>Fecha</th>
                            <th>Total</th>
                            <th>Efectivo</th>
                            <th>Tarjeta</th>
                            <th>Caja</th>
                        </tr>
                    </thead>
                    <tbody>";
        $rowventas=0;
        foreach ($resultadoc->result() as $fila) { 
            $table .= "<tr>
                            <td>".$fila->id_venta."</td>
                            <td>".$fila->vendedor."</td>
                            <td>".$fila->Nom."</td>
                            <td>".$fila->reg."</td>
                            <td>$ ".number_format($fila->monto_total,2,'.',',')."</td>
                            <td>$ ".number_format($fila->efectivo,2,'.',',')."</td>
                            <td>$ ".number_format($fila->pagotarjeta,2,'.',',')."</td>
                            <td>Caja ".$fila->caja."</td>
                        </tr>";
                        $rowventas++;
        }
        $table.="</tbody> </table>";

        $table2="<table class='table table-striped table-bordered table-hover' id='sample_2'>
                    <thead>
                        <tr>
                            <th>Turno</th>
                            <th>Apertura</th>
                            <th>Cierre</th>
                            <th>Utilidad</th>
                        </tr>
                    </thead>
                    <tbody>";
        $respuestaturno=$this->ModeloVentas->consultarturnoname($inicio,$fin);
        $obedt=0;
        //foreach ($respuestaturno->result() as $fila) { 
            /*if ($fila->fechacierre=='0000-00-00') {
                $horac =date('H:i:s');
                $fechac =date('Y-m-d');
            }else{
                $horac =$fila->horac;
                $fechac =$fila->fechacierre;
            }
            $fecha =$fila->fecha;
            $horaa =$fila->horaa; */
            /*$totalventas=$this->ModeloVentas->consultartotalturno($fecha,$horaa,$horac,$fechac,$personal);
            $totalpreciocompra=$this->ModeloVentas->consultartotalturno2($fecha,$horaa,$horac,$fechac,$personal);*/

            $totalventas=$this->ModeloVentas->consultartotalturno1($inicio,$fin,$personal);
            $totalpreciocompra=$this->ModeloVentas->consultartotalturno2_1($inicio,$fin,$personal);

            $obed =$totalventas-$totalpreciocompra;

            /*$table2 .= "<tr>
                            <td>".$fila->nombre."</td>
                            <td>".$fila->horaa."</td>
                            <td>".$fila->horac."</td>
                            <td>".number_format($obed,2,'.',',')."</td>
                        </tr>";*/
            $obedt=$obedt+$obed;
        //}
        $table2.="</tbody> </table>";

        $table3="<table class='table table-striped table-bordered table-hover' id='sample_3'>
                    <thead>
                        <tr>
                            <th>No. venta</th>
                            <th>Cajero</th>
                            <th>Cliente</th>
                            <th>Fecha</th>
                            <th>Total</th>
                            <th>Efectivo</th>
                            <th>Tarjeta</th>
                            <th>Caja</th>
                        </tr>
                    </thead>
                    <tbody>";
        foreach ($resultadovc->result() as $fila) { 
            $table3 .= "<tr>
                            <td>".$fila->id_venta."</td>
                            <td>".$fila->vendedor."</td>
                            <td>".$fila->Nom."</td>
                            <td>".$fila->reg."</td>
                            <td>$ ".number_format($fila->monto_total,2,'.',',')."</td>
                            <td>$ ".number_format($fila->efectivo,2,'.',',')."</td>
                            <td>$ ".number_format($fila->pagotarjeta,2,'.',',')."</td>
                            <td>Caja ".$fila->caja."</td>
                        </tr>";
                        $rowventas++;
        }
        $table3.="</tbody> </table>";

        //==================================================
            $table5="<table class='table table-striped table-bordered table-hover' id='sample_5'>
                        <thead>
                            <tr>
                                <th>Producto</th>
                                <th>Cantidad</th>
                            </tr>
                        </thead>
                        <tbody>";
            
            foreach ($productomayor->result() as $fila) { 
                $table5 .= "<tr>
                                <td>".$fila->producto."</td>
                                
                                <td>".number_format($fila->cantidad,2,'.',',')."</td>
                                
                            </tr>";
            }
            $table5.="</tbody> </table>";
        //==================================================

        $total=0;
        $totalvc=0;
        $subtotal=0;
        foreach ($resultadocs->result() as $fila) {
            $total = $fila->total;
            $subtotal = $fila->subtotal;
            $descuento = $fila->descuento;
        }

        foreach ($resultadocsvc->result() as $fila) {
            $totalvc = $fila->total;
            $subtotalvc = $fila->subtotal;
            $descuentovc = $fila->descuento;
        }

        $total = round($total,2);
        $total=number_format($total,2,'.',',');
        $subtotal = round($subtotal,2);
        $subtotal=number_format($subtotal,2,'.',',');
        $obedt=number_format($obedt,2,'.',',');
        $efectivo=number_format($efectivo,2,'.',',');
        $tarjetas=number_format($tarjetas,2,'.',',');
        $totalvc = round($totalvc,2);
        $totalvc=number_format($totalvc,2,'.',',');

        $totalGral=$total+$totalvc;
        $totalGral=number_format($totalGral,2,'.',',');

        $array = array("tabla"=>$table,
                        //"tabla2"=>$table2,
                        "tabla3"=>$table3,
                        "tabla5"=>$table5,
                        "dTotal"=>"".$total."",
                        "dTotalvc"=>"".$totalvc."",
                        "totalventas"=>$rowventas,
                        "totalefectivo"=>$efectivo,
                        "totaltarjetas"=>$tarjetas,
                        "totalGral"=>$totalGral,
                        "dImpuestos"=>0,
                        "totalutilidad"=>"".$obedt."",
                        "descuento"=>$descuento,
                        "dSubtotal"=>"".$subtotal."",
                    );
            echo json_encode($array);
    }
    

       
    
}
