<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Verificador extends CI_Controller {

	public function __construct() {
        parent::__construct();
        $this->load->helper('url');
        $this->load->model('ModeloVerificador');
        /*if (!$this->session->userdata('logeado')){
          redirect('/Login');
	    }else{
	        $this->perfilid=$this->session->userdata('perfilid');
	        //ira el permiso del modulo
	        $this->load->model('Login_model');
	        $permiso=$this->Login_model->getviewpermiso($this->perfilid,7);// 7 es el id del submenu
	        if ($permiso==0) {
	            redirect('/Sistema');
	        }
	    }*/
    }

	public function index(){
		$this->load->view('templates/header');
        $this->load->view('templates/navbar');
		$this->load->view('verificador/busqueda');
		$this->load->view('templates/footer');
        $this->load->view('verificador/busquedajs');
	}

	public function getDatosProd(){
	    $prod =$this->input->post('prod');
	    $results=$this->ModeloVerificador->getDatos($prod);
	    echo json_encode($results);
  	}

	public function datatable_records(){
		$params = $this->input->post();
		$datas = $this->ModeloVerificador->get_similaresProd($params["id"],$params["substancia"],$params["idsubstancia"]);
        $json_data = array("data" => $datas);
        echo json_encode($json_data);
	}

}
