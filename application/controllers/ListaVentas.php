<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ListaVentas extends CI_Controller {
    function __construct(){
        parent::__construct();
        $this->load->helper('url');
        $this->load->model('ModeloVentas');
        $this->load->model('ModeloGeneral');
    }

    public function index(){
            $pages=10; //Número de registros mostrados por páginas
            $this->load->library('pagination'); //Cargamos la librería de paginación
            $config['base_url'] = base_url().'ListaVentas/view/'; // parametro base de la aplicación, si tenemos un .htaccess nos evitamos el index.php
            $config['total_rows'] = $this->ModeloVentas->filas();//calcula el número de filas
            $config['per_page'] = $pages; //Número de registros mostrados por páginas  
            $config['num_links'] = 3; //Número de links mostrados en la paginación
            $config['first_link'] = 'Primera';//primer link
            $config['last_link'] = 'Última';//último link
            $config["uri_segment"] = 3;//el segmento de la paginación
            $config['next_link'] = 'Siguiente';//siguiente link
            $config['prev_link'] = 'Anterior';//anterior link
            $this->pagination->initialize($config); //inicializamos la paginación 
            $pagex = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
            $data["ventas"] = $this->ModeloVentas->total_paginados($pagex,$config['per_page']);
            
            $this->load->view('templates/header');
            $this->load->view('templates/navbar');
            //$this->load->view('Personal/Personal',$data);
            $this->load->view('ventas/listaventas',$data);
            $this->load->view('templates/footer');
    }

    function cancalarventa(){
        $id = $this->input->post('id');
        $this->ModeloVentas->cancalarventa($id);

        $resultado=$this->ModeloVentas->ventadetalles($id);
        foreach ($resultado->result() as $item){
            $this->ModeloVentas->regresarpro($item->id_producto,$item->cantidad);
        }
    }

    function buscarvent(){
        $buscar = $this->input->post('buscar');
        $resultado=$this->ModeloVentas->ventassearch($buscar);
        foreach ($resultado->result() as $item){ ?>
            <tr id="trven_<?php echo $item->id_venta; ?>">
                  <td><?php echo $item->id_venta; ?></td>
                  <td><?php echo $item->reg; ?></td>
                  <td><?php echo $item->vendedor; ?></td>
                  <td><?php echo $item->monto_total; ?></td>
                  <td><?php echo $item->cliente; ?></td>
                  <td><?php if($item->cancelado==1){ echo '<span class="badge badge-danger">Cancelado</span>';} ?></td>
                  <td>
                    <button class="btn btn-raised gradient-blackberry white sidebar-shadow" onclick="ticket(<?php echo $item->id_venta; ?>)" title="Ticket" data-toggle="tooltip" data-placement="top">
                      <i class="fa fa-book"></i>
                    </button>
                    <button class="btn btn-raised gradient-flickr white sidebar-shadow" onclick="cancelar(<?php echo $item->id_venta; ?>,<?php echo $item->monto_total; ?>)" title="Cancelar" data-toggle="tooltip" data-placement="top" <?php if($item->cancelado==1){ echo 'disabled';} ?>>
                      <i class="fa fa-times"></i>
                    </button>
                  </td>
          </tr>
        <?php }
    }

    public function Credito(){
        $this->load->view('templates/header');
        $this->load->view('templates/navbar');
        $this->load->view('credito/listaCredito');
        $this->load->view('templates/footer');
        $this->load->view('credito/listaCreditojs');
    }

    public function ProducListDetalle(){
        $id=$this->input->post('id');
        //$where="id_cliente=".$id." AND pagado=0 AND metodo=4";
        $data=$this->ModeloVentas->GetTableProductos($id);
        echo json_encode($data);
    }

    public function ProducList(){
        $id=$this->input->post('id');
        $where="id_cliente=".$id." AND pagado=0 AND metodo=4";
        $data=$this->ModeloGeneral->get_recordWeAll($where,"ventas");
        echo json_encode($data);
    }

    public function PagarVentaC(){
        date_default_timezone_set('America/Mexico_City');
        $id=$this->input->post('id');
        $fecha=date('Y-m-d H:i:s');
        $data = array('pagado' => 1, "fecha_pago"=>$fecha);
        $this->ModeloGeneral->edit_record($id,"id_venta",$data,"ventas");
    }
       
}
