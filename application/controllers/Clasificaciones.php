<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Clasificaciones extends CI_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->helper('url');
		$this->load->model('ModeloGeneral');
		$this->load->model('ModeloCatalogos');
	}

	public function index(){
		$this->load->view('templates/header');
        $this->load->view('templates/navbar');
        $this->load->view('clasificaciones/listado');
        $this->load->view('templates/footer');
        $this->load->view('clasificaciones/listadojs');
	}

	public function alta($id=0){
		if ($id>0){
			$info=$this->ModeloGeneral->get_record($id,"clasificacion");
			$data['id']=$info->id;
			$data['nombre']=$info->nombre;
		}else{
			$data['id']="";
			$data['nombre']="";
		}
		$this->load->view('templates/header');
        $this->load->view('templates/navbar');
        $this->load->view('clasificaciones/alta',$data);
        $this->load->view('templates/footer');
        $this->load->view('clasificaciones/altajs');
	}

	public function addcla(){
		$nombre=$this->input->post('nombre');
		$id=$this->input->post('id');
		$data=array('id'=>$id,'nombre'=>$nombre);
		$ret="";
		if ($id==""){
			unset($data['id']);
			$ret=$this->ModeloGeneral->add_record($data,"clasificacion");
		}else{
			$this->ModeloGeneral->edit_record($id,"id",$data,"clasificacion");
			$ret=$id;
		}
		echo $ret;
	}

	public function delete($id){
		$this->ModeloGeneral->edit_record($id,"id",array("activo"=>0),"clasificacion");
	}

	public function getClas(){
		$params = $this->input->post();
        $getdata = $this->ModeloCatalogos->GetElementsClasi($params);
        $result=$getdata->result();
        $totaldata= count($result); 
        $json_data = array(
            "draw"            => intval( $params['draw'] ),   
            "recordsTotal"    => intval($totaldata),  
            "recordsFiltered" => intval($totaldata),
            "data"            => $result,
            "query"           =>$this->db->last_query()   
        );
        echo json_encode($json_data);
	}

}

/* End of file Clasificaciones.php */
/* Location: ./application/controllers/Clasificaciones.php */