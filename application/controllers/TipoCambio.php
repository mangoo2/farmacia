<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class TipoCambio extends CI_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->helper('url');
		$this->load->model('ModeloGeneral');
	}

	public function index(){
		$data["cambio"]=$this->ModeloGeneral->SimpleGetC("cambio","status=1","valdolar");
		$this->load->view('templates/header');
        $this->load->view('templates/navbar');
        $this->load->view('config/contipoc',$data);
        $this->load->view('templates/footer');
        $this->load->view('config/contipocjs');
	}

	public function Guardar(){
		$val=$this->input->post('value');
		date_default_timezone_set('America/Mexico_City');
		$fecha=date('Y-m-d H:i:s');
		$data=array('valdolar'=> $val,'fecha'=>$fecha);
		$n=$this->ModeloGeneral->add_record($data,"cambio");
		$info=array('status'=>0);
		$where="id<".$n;
		$this->ModeloGeneral->edit_recordW("cambio",$info,$where);
		echo $n;
	}

}

/* End of file TipoCambio.php */
/* Location: ./application/controllers/TipoCambio.php */