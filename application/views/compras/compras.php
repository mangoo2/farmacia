<div class="row">
    <div class="col-md-12">
      <h2>Compras </h2>
    </div>
</div>
<!--Statistics cards Ends-->
<!--Line with Area Chart 1 Starts-->
<div class="row">
<div class="col-sm-12">
    <div class="card">
        <div class="card-header">
            <h4 class="card-title">Compras</h4>
        </div>
        <div class="card-body">
            <div class="card-block">
                <!--------//////////////-------->
                <div class="row">
                    <div class="col-md-12">
                        <!--<input type="checkbox" name="checkimprimir" id="checkimprimir" > <label for="checkimprimir"> Imprimir Ticket</label>-->
                    </div>
                    <div class="col-md-9">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="control-label">Proveedor:</label>
                                <select class="form-control" id="cproveedor" name="cproveedor"></select>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <label class="control-label">Cantidad:</label>
                                <input type="number" name="vcantidad" id="vcantidad" class="form-control" min="0">
                            </div>
                        </div>
                        <div class="col-md-8">
                            <div class="form-group">
                                <label class="control-label"><i class="fa fa-barcode"></i> Producto/Código:</label>
                                <select class="form-control" id="vproducto" name="vproducto"></select>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <label class="control-label">Precio compra:</label>
                                <input type="number" name="cprecio" id="cprecio" class="form-control" min="0">
                            </div>
                        </div>
                        <div class="col-md-12">
                            <br>
                        </div>
                        <div class="col-md-12">
                            <table class="table table-hover" id="productosv">
                                <thead>
                                    <tr>
                                        <th>Clave</th>
                                        <th>Cantidad</th>
                                        <th>Producto</th>
                                        <th>Precio/U</th>
                                        <th>Total</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody id="class_productos">
                                    
                                </tbody>
                                </table>
                            
                        </div>
                        <div class="col-md-12">
                            <a href="#" class="btn btn-raised gradient-green-tea white sidebar-shadow" onclick="addproducto()">Agregar producto</a>
                            <a href="#" class="btn btn-raised gradient-blackberry white sidebar-shadow" onclick="limpiar()">Limpiar</a>
                            
                        </div>  
                    </div>
                    <div class="col-md-3">
                        
                        
                        
                        
                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="control-label">Total:</label>
                                <input type="number" name="vtotal" id="vtotal" class="form-control" readonly>
                                <input type="hidden" name="vsbtotal" id="vsbtotal" class="form-control" readonly>
                            </div>
                        </div>
                        
                        <div class="col-md-12">
                            <a href="#" class="btn btn-raised gradient-green-tea white sidebar-shadow" id="ingresaventa">Ingresar Compra</a>
                        </div>
                    </div>
                    
                    
                </div>
        <!--------//////////////-------->
            </div>
        </div>
    </div>
</div>
</div>
<style type="text/css">
    #iframereporte{
        background: white;
    }
    iframe{
        height: 500px;
        border:0;
        width: 100%;
    }
</style>
<div class="modal fade text-left" id="iframeri" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel1">Ticket</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <!--<div class="modal-body">-->
            <div id="iframereporte"></div>
            <!--</div>-->
            <div class="modal-footer">
                <button type="button" class="btn grey btn-outline-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>