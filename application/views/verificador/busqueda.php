<style type="text/css">

.main {
    width: 50%;
    margin: 50px auto;
}

.has-search .form-control {
    padding-left: 2.375rem;
}

.has-search .form-control-feedback {
    position: absolute;
    z-index: 2;
    display: block;
    width: 2.375rem;
    height: 2.375rem;
    line-height: 2.375rem;
    text-align: center;
    pointer-events: none;
    color: #aaa;
}
</style>
<script src="http://code.jquery.com/jquery-1.9.1.js"></script>
<link href="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.css" rel="stylesheet" type="text/css">
<script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.24/css/jquery.dataTables.min.css">
<script src="https://cdn.datatables.net/1.10.24/js/jquery.dataTables.min.js"></script>
<div class="row">
  <div class="col-md-12">
    <h2>Verificador de Precios</h2>
  </div>
  
  <div class="col-md-12">
    <div class="col-md-10"></div>
  </div>
  
  
</div>
<div class="row">
  <div class="col-sm-12">
    <div class="card">
      <div class="card-body">
        <div class="card-block">
          <h4 id="name">Nombre: </h4>
          <h4 id="subs">Substancia: </h4>
          <h4 id="pre">Precio: </h4>
          <h4 id="exis">Existencia: </h4>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="row">
  <div class="col-sm-12">
    <div class="card">
      <div class="card-header">
          <h4 class="card-title">Búsqueda de productos</h4>
      </div>
      
      <div class="card-body">
        <div class="card-block">
          <!--------//////////////-------->
          <div class="col-md-10">
            <div class="input-group" style="height: 50px">
              <input type="text" class="form-control" placeholder="Búsqueda de producto" autofocus="" id="prod_search" autocomplete>
              <div class="input-group-append" style="height: 50px">
                <button class="btn btn-secondary" type="button">
                  <i class="fa fa-search" style="height: 30px"></i>
                </button>
              </div>
            </div>
          </div>
          

          <div class="col-md-12" id="cont_search" style="display: none">
            <br>
            <h4 class="card-title">Listado de Búsqueda de productos y/o similares</h4>
            <table class="table table-striped" id="tabla_tipos" style="width: 100%">
              <thead>
                <tr>
                  <th>Producto</th>
                  <th>Precio</th>
                  <!--<th>Presentación</th>-->
                  <th>Stock</th>
                  <th>Substancia</th>
                </tr>
              </thead>
              <tbody>

                
              </tbody>
            </table>
          </div>
          <div class="col-md-10" style="height: 350px">

          </div>

        </div>
      </div>
    </div>
  </div>
</div>

