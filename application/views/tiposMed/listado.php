
<div class="row">
  <div class="col-md-12">
    <h2>Tipo de Medicamento</h2>
  </div>
  
  <div class="col-md-12">
    <div class="col-md-10"></div>
    <div class="col-md-2">
      <a href="<?php echo base_url(); ?>TipoMedicamento/alta" class="btn btn-raised gradient-ibiza-sunset white sidebar-shadow"><i class="fa fa-plus"></i></span> Nuevo</a>
    </div>
  </div>
  
  
</div>
<!--Statistics cards Ends-->

<!--Line with Area Chart 1 Starts-->
<div class="row">
  <div class="col-sm-12">
    <div class="card">
      <div class="card-header">
          <h4 class="card-title">Listado de tipos</h4>
      </div>
      
      <div class="card-body">
        <div class="card-block">
            <!--------//////////////-------->
            <table class="table table-striped" id="tabla_tipos" style="width: 100%">
              <thead>
                <tr>
                  <th>#</th>
                  <th>Nombre</th>
                  <th></th>
                </tr>
              </thead>
              <tbody>

                
              </tbody>
            </table>
          </div>
      </div>
    </div>
  </div>
</div>
<!------------------------------------------------>

<div class="modal fade text-left" id="eliminacion" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel1">Mensaje de confirmaci&oacute;n</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                ¿Desea <b>Eliminar</b> el tipo ?
                      <input type="hidden" id="hddIdp">
            </div>
            <div class="modal-footer">
                <button type="button" id="sieliminar" class="btn btn-raised gradient-purple-bliss white" data-dismiss="modal">Aceptar</button>
                <button type="button" class="btn btn-raised gradient-ibiza-sunset white" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>
