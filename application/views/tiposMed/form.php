<div class="row">
    <div class="col-md-12">
      <h2>Tipos de Medicamento </h2>
    </div>
</div>
<!--Statistics cards Ends-->
<!--Line with Area Chart 1 Starts-->
<div class="row">
    <div class="col-sm-12">
        <div class="card">
            <div class="card-header">
                <h4 class="card-title">Tipos de Medicamento</h4>
            </div>
            <form id="form_tipos">
                <?php if(isset($tipos)){
                    echo "<input name='id' id='id' value='$tipos->id' hidden />";
                } ?>
                <div class="card-body">
                    <div class="card-block">
                        <!--------//////////////-------->
                        <div class="row">
                            <div class="col-md-12">
                                <!--<input type="checkbox" name="checkimprimir" id="checkimprimir" > <label for="checkimprimir"> Imprimir Ticket</label>-->
                            </div>
                            <div class="col-md-9">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label">Nombre:</label>
                                        <input type="text" name="nombre" id="nombre" class="form-control" <?php if(isset($tipos)){ echo "value='$tipos->nombre'"; } ?>>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-actions">
                            <div class="card-body">
                                <button type="submit" class="btn btn-success" id="btn_submit"> <i class="fa fa-check"></i> Guardar</button>
                                <button type="button" class="btn btn-dark">Cancel</button>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
