<div class="row">
  <div class="col-md-12">
    <h2>Lista ventas</h2>
  </div>
</div>
<!--Statistics cards Ends-->

<!--Line with Area Chart 1 Starts-->
<div class="row">
<div class="col-sm-12">
  <div class="card">
    <div class="card-header">
        <h4 class="card-title">Listado de ventas a crédito</h4>
    </div>
    <div class="col-md-12">
      <div class="col-md-8">
        
      </div>
    </div>
    <div class="card-body">
        <div class="card-block">
            <!--------//////////////-------->
        <table class="table table-striped" id="data-tables" style="width: 100%">
            <thead>
              <tr>
                <th>Folio</th>
                <th>Cliente</th>
                <th>Monto</th>
                <th></th>
              </tr>
            </thead>
          </table>
        </div>
                  </div>
              </div>
          </div>
        </div>
<!------------------------------------------------>
<style type="text/css">
    #iframereporte{
        background: white;
    }
    iframe{
        height: 500px;
        border:0;
        width: 100%;
    }
</style>

<div class="modal fade text-left" id="iframeri" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel1">Ticket</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body row">
              <div class="col-md-12" id="iframereporte">
                <table class="table">
                  <tr>
                    <th>Fecha</th>
                    <th>Monto</th>
                    <th>.</th>
                  </tr>
                  <tbody id="table-venta"></tbody>
                </table>
              </div>
              <div class="col-md-4"></div>
              <div class="col-md-4">
                <label id="Saldo">Saldo: </label>
              </div>
              <div class="col-md-4"></div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-raised gradient-ibiza-sunset white" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade text-left" id="cancelar" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel1">Mensaje de confirmaci&oacute;n</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                ¿Desea cancelar el ticket No. <span id="NoTicket"></span> por un total de <span id="CantidadTicket"></span>?
                      <input type="hidden" id="hddIdVenta">
            </div>
            <div class="modal-footer">
                <button type="button" id="sicancelar" class="btn btn-raised gradient-purple-bliss white" data-dismiss="modal">Aceptar</button>
                <button type="button" class="btn btn-raised gradient-ibiza-sunset white" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>
<!---->