<?php

    require_once('TCPDF/examples/tcpdf_include.php');
    require_once('TCPDF/tcpdf.php');
    $this->load->helper('url');
    foreach ($configticket->result() as $item){
      $GLOBALS['titulo'] = $item->titulo;
      $GLOBALS['mensaje'] = $item->mensaje;
      $GLOBALS['mensaje2'] = $item->mensaje2;
      $fuente = $item->fuente;
      $GLOBALS['tamano'] = $item->tamano;
      $GLOBALS['margensup'] = $item->margensup;
    }
    foreach ($getventas->result() as $item){
      $GLOBALS['idticket']= $item->id_venta;
      $id_personal = $item->id_personal;
      $id_cliente = $item->id_cliente;
      $metodo = $item->metodo;
      $GLOBALS['subtotal'] = $item->subtotal;
      $GLOBALS['descuento'] = $item->descuento;
      $GLOBALS['monto_total'] = $item->monto_total;
      $reg = $item->reg;
    }
    $GLOBALS['fecha']= date("d-m-Y",strtotime($reg));
    $GLOBALS['hora']= date("G:i",strtotime($reg));
    if ($fuente==1) {
      $GLOBALS['tipofuente'] = "arial";
    }
    if ($fuente==2) {
      $GLOBALS['tipofuente'] = "Times New Roman";
    }
    if ($fuente==3) {
      $GLOBALS['tipofuente'] = "Open Sans";
    }
    if ($fuente==4) {
      $GLOBALS['tipofuente'] = "Calibri";
    }
//=======================================================================================
class MYPDF extends TCPDF {
  //Page header
  public function Header() {
      //$img_header = 'header.jpg';
      //$this->Image($img_header, 0, 0, 0, 197, '', '', '', false, 100, '', false, false, 0);
      //$this->Image($img_header, 0, 0, 210, '', 'JPG', '', false, false, 300, '', false, false, 0, false, false, false);
      $imglogo = base_url().'public/img/ops.png';
      $html = '
          <table width="100%" border="0" >
            <tr>
              <td colspan="3" align="center"><img src="'.$imglogo.'" width="130px" ></td>
            </tr>
            <tr>
              <th colspan="3" align="center" style=" font-size: '.$GLOBALS['tamano'].';text-align: left; font-family: '.$GLOBALS['tipofuente'].';">'.$GLOBALS['titulo'].'</th>
            </tr>
            <tr>
                <th colspan="3" align="center" ></th>
            </tr>
            <tr>
                <th colspan="3" align="center" style="font-size: '.$GLOBALS['tamano'].'; font-family: '.$GLOBALS['tipofuente'].';">'.$GLOBALS['mensaje'].'</th>
            </tr>
            <tr>
                <th colspan="3" align="center" ></th>
            </tr>
            <tr>
                <th colspan="3" style="font-size: '.$GLOBALS['tamano'].'; font-family: '.$GLOBALS['tipofuente'].';">Fecha: '.$GLOBALS['fecha'].'</th>
            </tr>
            <tr>
                <th colspan="3" style="font-size: '.$GLOBALS['tamano'].'; ;font-family: '.$GLOBALS['tipofuente'].';">Hora: '.$GLOBALS['hora'].'</th>
            </tr>
            <tr>
                <th colspan="3" style="font-size: '.$GLOBALS['tamano'].'; ;font-family: '.$GLOBALS['tipofuente'].';">No ticket: '.$GLOBALS['idticket'].'</th>
            </tr>
            <tr>
                <th colspan="3" align="center" style="font-size: 9"></th>
            </tr>
          </table>';
        $this->writeHTML($html, true, false, true, false, '');
  }
    // Page footer
  public function Footer() {
      $html = ' 
      <table width="100%" border="0">
        <tr>
          <td align="right" class="footerpage">Pagina '.$this->getAliasNumPage().' de '.$this->getAliasNbPages().'</td>
        </tr>
      </table>';
      //$this->writeHTML($html, true, false, true, false, '');
  }
} 
$pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, array(58, 210), true, 'UTF-8', false);

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('agb');
$pdf->SetTitle('Ticket');
$pdf->SetSubject('Ticket');
$pdf->SetKeywords('Ticket');

// set default header data
$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);

// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
$pdf->SetMargins('6', '72', '6');
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

// set auto page breaks
$pdf->SetAutoPageBreak(true, PDF_MARGIN_BOTTOM);

// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

$pdf->SetFont('dejavusans', '', 9);
// add a page
$pdf->AddPage();
$html='<table border="0" style="margin-top:0;margin-bottom:0;"><tr>
                <th width="20%" style="font-size: 7; font-family: '.$GLOBALS['tipofuente'].';" align="center">Cant.</th>
                <th width="50%" style="font-size: 7; font-family: '.$GLOBALS['tipofuente'].';" align="center">Articulo</th>
                <th width="30%" style="font-size: 7; font-family: '.$GLOBALS['tipofuente'].';" align="center">Precio</th>
            </tr>';
foreach ($getventasd->result() as $rowEmp){
    $html .= '
                <tr>
                    <th style="font-size: 7" align="center">'.$rowEmp->cantidad.'</th>
                    <th style="font-size: 7" align="center">'.$rowEmp->nombre.'</th>
                    <th style="font-size: 7" align="center">'.$rowEmp->precio.'</th>
                </tr>
                ';
}

$html .= '<tr>
                <th colspan="3" align="center" style="font-size: 9"></th>
            </tr></table>
            <table border="0" style="margin-top:0;margin-bottom:0;">
                <tr>
                    <th style="font-size: 7" align="center"></th>
                    <th style="font-size: 7" align="center">Subtotal</th>
                    <th style="font-size: 7" align="center">'.number_format($GLOBALS['subtotal'],2,'.',',').'</th>
                </tr>
                <tr>
                    <th style="font-size: 7" align="center"></th>
                    <th style="font-size: 7" align="center">Descuento</th>
                    <th style="font-size: 7" align="center">'.number_format($GLOBALS['descuento'],2,'.',',').'</th>
                </tr>
                <tr>
                    <th style="font-size: 7" align="center"></th>
                    <th style="font-size: 7" align="center">Total</th>
                    <th style="font-size: 7" align="center">'.number_format($GLOBALS['monto_total'],2,'.',',').'</th>
                </tr>
                <tr>
                  <th colspan="3"></th>
                </tr>
                <tr>
                  <th colspan="3" align="Center" style="font-size: '.$GLOBALS['tamano'].'; font-family: '.$GLOBALS['tipofuente'].';">'.$GLOBALS['mensaje2'].'</th>
                </tr>
                </table>';
      $pdf->writeHTML($html, true, false, true, false, '');
$pdf->IncludeJS('print(true);');
$pdf->Output('Captura.pdf', 'I');

//$pdf->Output('files/'.$GLOBALS["carpeta"].'/facturas/'.$GLOBALS["rrfc"].'_'.$GLOBALS["Folio"].'.pdf', 'F');

?>