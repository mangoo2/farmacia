<style type="text/css">
  .vd_red{
    font-weight: bold;
    color: red;
    margin-bottom: 5px;
  }
  .vd_green{
    color: #009688;
  }
  input,select,textarea{
    margin-bottom: 15px;
  }

.main {
    width: 50%;
    margin: 50px auto;
}

.has-search .form-control {
    padding-left: 2.375rem;
}

.has-search .form-control-feedback {
    position: absolute;
    z-index: 2;
    display: block;
    width: 2.375rem;
    height: 2.375rem;
    line-height: 2.375rem;
    text-align: center;
    pointer-events: none;
    color: #aaa;
}
</style>
<input type="hidden" name="productoid" id="productype" value="<?php echo $productype;?>">
<input type="hidden" name="productoid" id="productoid" value="<?php echo $id;?>">
<div class="row">
  <div class="col-md-12">
    <h2>Producto</h2>
  </div>
</div>
<!--Statistics cards Ends-->
<!--Line with Area Chart 1 Starts-->
<div class="row">
  <div class="col-sm-12">
      <div class="card">
          <div class="card-header">
              <h4 class="card-title"><?php echo $label;?></h4>
          </div>
          <div class="card-body">
              <div class="card-block form-horizontal">
                  <div class="row">
                    <div class="col-md-3 form-group">
                      <label>Tipo Producto</label>
                      <select class="form-control" id="tpp">
                        <option value="1">Farmacia</option>
                        <option value="2">Tienda</option>
                      </select>
                    </div>
                    <div class="col-md-2">
                      <br>
                      <br>
                      <div class="form-check">
                        <input class="form-check-input" type="checkbox" id="CS">
                        <label class="form-check-label">Servicio</label>
                      </div>
                    </div>
                  </div>
                  <!--------//////////////-------->
                    <div class="row" id="ServiceS">
                      <div class="col-md-12">
                      <form id="formServices">
                        <div class="row">
                          <div class="col-md-3 form-group">
                            <label class="col-md-3 control-label">Clave:</label>
                            <div class=" col-md-7">
                              <input class="form-control" type="text" value=" <?php echo $codigo; ?> " name="codigo">
                            </div>
                          </div>
                          <div class="col-md-9 form-group">
                            <label  class="col-md-2 control-label">Nombre comercial:</label>
                            <div class=" col-md-10">
                              <input class="form-control" type="text" value=" <?php echo $nombre; ?> " name="nombre">
                            </div>
                          </div>
                        </div>
                        </form>
                        </div>
                    </div>
                  <div class="row" id="ProductSF">
                    <form method="post"  role="form" id="formproductos">
                      <div class="col-md-12">
                        <div class="col-md-12">
                            <div class="form-group">
                              <label class="col-md-1 control-label">código:</label>
                              <div class=" col-md-4">
                                <input type="text" class="form-control" name="codigo" value="<?php echo $codigo;?>" aria-describedby="button-addon2">
                              </div>
                              <div class="col-md-2">
                                <a href="#" class="btn btn-raised btn-primary" id="generacodigo" >Generar</a>
                              </div>
                              <div class="col-md-3" style="display: none;">
                                <input type="checkbox" name="productofiscal" id="productofiscal" <?php  if($productofiscal==1){ echo 'checked';}?> >
                                <label for="productofiscal">Producto fiscal</label>
                              </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                          <div class="form-group">
                            <label class="col-md-2 control-label">Nombre:</label>
                            <div class=" col-md-10 ">
                              <input type="text" class="form-control" name="nombre" value="<?php echo $nombre;?>">
                            </div>
                          </div>
                        </div>
                        <div class="col-md-5">
                          <div class="form-group">
                            <label class="col-md-3 control-label">Substancia:</label>
                            <div class=" col-md-9">
                              <select class="form-control" id="subsmed" name="substancia">
                              <?php foreach ($subEstancia as $key){ ?>
                                <option <?php if ($key->id==$substancia){ echo "selected"; } ?> value="<?php echo $key->id; ?>"><?php echo $key->nombre ?></option>
                              <?php } ?>
                            </select>
                            </div>
                          </div>
                        </div>
                        <div class="col-md-1">
                          <a class="btn btn-success" onclick="Substancia()"><i class="fa fa-plus"></i></a>
                        </div>
                        <div class="col-md-2">
                          <div class="form-group">
                            <label>Tipo</label>
                            <select class="form-control" id="tipmed" name="tipo">
                              <?php foreach ($tipomed as $key){ ?>
                                <option <?php if ($key->id==$tipo){ echo "selected"; } ?> value="<?php echo $key->id; ?>"><?php echo $key->nombre ?></option>
                              <?php } ?>
                            </select>
                          </div>
                        </div>
                        <div class="col-md-1">
                          <br>
                          <a class="btn btn-success" onclick="TipoMed()"><i class="fa fa-plus"></i></a>
                        </div>
                        <div class="col-md-2">
                          <div class="form-group">
                            <label>Concentración</label>
                            <input type="text" class="form-control" name="concentracion" value="<?php echo $concentracion; ?>">
                          </div>
                        </div>
                        <div class="col-md-2">
                          <div class="form-group">
                            <label>Piezas</label>
                            <input type="text" class="form-control" name="piezas" value="<?php echo $piezas; ?>">
                          </div>
                        </div>
                        <div class="col-md-2">
                          <div class="form-group">
                            <label>Artiículo</label>
                            <select class="form-control" name="articulo">
                              <option value="Generico">Genérico</option>
                              <option value="Patente">Patente</option>
                            </select>
                            <!--<input type="text" class="form-control" name="" value="<?php echo $articulo;?>">--->
                          </div>
                        </div>
                        <div class="col-md-2">
                          <div class="form-group">
                            <label>Clasificación:</label>
                            <select class="form-control" id="clamed" name="clasificacion">
                              <?php foreach ($clasificaciones as $key){ ?>
                                <option <?php if ($key->id==$clasificacion){ echo "selected"; } ?> value="<?php echo $key->id; ?>"><?php echo $key->nombre ?></option>
                              <?php } ?>
                            </select>
                          </div>
                        </div>
                        <div class="col-md-1">
                          <br>
                          <a class="btn btn-success" onclick="Clasif()"><i class="fa fa-plus"></i></a>
                        </div>
                        <div class="col-md-3">
                          <div class="form-group">
                            <label class="col-md-5 control-label">Stock mínimo:</label>
                            <div class=" col-md-7">
                              <input type="text" class="form-control" name="stockmin" value="<?php echo $stockmin;?>">
                            </div>
                          </div>
                        </div>
                        <div class="col-md-3">
                          <div class="form-group">
                            <label class="col-md-5 control-label">Existencias:</label>
                            <div class=" col-md-7">
                              <input type="text" class="form-control" name="stock" value="<?php echo $stock;?>">
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="col-md-12">
                        <h3>Precios</h3>
                        <hr />
                        <div class="col-md-12">
                            <div class="form-group">
                              <label class="col-md-2 control-label">Precio Compra:</label>
                              <div class=" col-md-3">
                                <input type="number" name="preciocompra" id="preciocompraF" class="form-control" value="<?php echo $preciocompra;?>" placeholder="">
                              </div>
                              <div class=" col-md-1" style="width: 47px;">
                                <button class="btn btn-raised">%</button>
                              </div>
                              <div class=" col-md-2">
                                <input type="number" class="form-control" name="porcentaje" id="porcentajeF"  aria-describedby="button-addon2" onchange="calcularF(this);" value="<?php echo $porcentaje;?>">
                              </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                              <label class="col-md-2 control-label">Precio venta:</label>
                              <div class=" col-md-3">
                                <input type="number" class="form-control" name="precioventa" id="precioventaF" value="<?php echo $precioventa;?>" placeholder="">
                              </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                              <label class="col-md-2 control-label">Precio mayoreo:</label>
                              <div class=" col-md-3">
                                <input type="number" class="form-control" name="mayoreo" id="preciomayoreoF" value="<?php echo $mayoreo;?>" placeholder="">
                              </div>
                              <div class=" col-md-2">
                                <input type="number" class="form-control" name="canmayoreo" id="cpmayoreoF" value="<?php echo $canmayoreo;?>" placeholder="a partir de # de unidades">
                              </div>
                            </div>
                        </div>
                      </div>
                    </form>
                  </div>
                  <div class="row" id="poveedoresF">
                    <div class="col-md-12">
                      <h3>Proveedores</h3>
                      <hr/>
                      <div class="row">
                        <div class="col-md-10" id="CFPF">
                          <form id="FRMP-0">
                            <input type="hidden" name="idd">
                            <div class="col-md-5 form-group">
                              <label>Proveedor</label>
                              <select class="form-control" id="prove" name="id_proveedor">
                              <?php foreach ($provedores as $key):?>
                                <option value="<?php echo $key->id_proveedor; ?>"><?php echo $key->razon_social; ?></option>
                              <?php endforeach ?>
                            </select>
                            </div>
                            <div class="col-md-5 form-group">
                              <label>Último Precio de compra</label>
                              <input class="form-control" type="text" onblur="Moneda(this)" name="precio">
                            </div>
                            <div class="col-md-2">
                              <br>
                              <button onclick="DeletePF(this)" type="button" class="btn btn-danger"><i class="fa fa-minus"></i></button>
                            </div>
                          </form>
                        </div>
                        <div class="col-md-2">
                          <br>
                          <button type="button" class="btn btn-raised btn-primary" onclick="CloneElement(0,0,'')">otro</button>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="row" id="Similares">
                    <div class="col-md-12">
                      <h3>Similares</h3>
                      <hr/>
                      <div class="row">
                        <div class="col-md-6" id="CSF">
                          <form id="FSF-0">
                            <input type="hidden" name="idd">
                            <div class="col-md-10 form-group">
                              <label>Nombre Comercial</label>
                              <select name="similar[]" class="form-control selectorM" multiple="multiple">
                                <?php foreach ($prodParSim as $key ): ?>
                                  <option <?php if (in_array($key->productoid,$charged)){ echo 'selected="selected"'; } ?> value="<?php echo $key->productoid; ?>"><?php echo $key->nombre; ?></option>
                                <?php endforeach ?>
                              </select>
                            </div>
                          </form>
                        </div>
                        <div class="col-md-4"></div>
                        <div class="col-md-2">
                          <br>
                          <!--onclick="CloneSimilar(0,'')"-->
                          <button type="button" class="btn btn-raised btn-primary" >otro</button>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="row" id="ProductST">
                    <div class="col-md-12">
                      <form id="formtienda">
                        <div class="col-md-12">
                            <div class="form-group">
                              <label class="col-md-1 control-label">código:</label>
                              <div class=" col-md-4">
                                <input type="text" class="form-control" name="codigo" value="<?php echo $codigo;?>" aria-describedby="button-addon2">
                              </div>
                              <div class="col-md-2">
                                <a href="#" class="btn btn-raised btn-primary" id="generacodigo" >Generar</a>
                              </div>
                              <div class="col-md-3" style="display: none;">
                                <input type="checkbox" name="productofiscal" id="productofiscal" <?php  if($productofiscal==1){ echo 'checked';}?> >
                                <label for="productofiscal">Producto fiscal</label>
                              </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                          <div class="form-group">
                            <label class="col-md-1 control-label">Nombre comercial:</label>
                            <div class=" col-md-9 ">
                              <input type="text" class="form-control" name="nombre" value="<?php echo $nombre;?>">
                            </div>
                          </div>
                        </div>
                        <div class="col-md-5">
                          <div class="form-group">
                            <label class="col-md-3 control-label">Categoría:</label>
                            <div class=" col-md-9">
                              <select class="form-control" id="catpro" name="categoria">
                              <?php foreach ($categorias as $key){ ?>
                                <option <?php if ($key->categoriaId==$categoria){ echo "selected"; } ?> value="<?php echo $key->categoriaId; ?>"><?php echo $key->categoria ?></option>
                              <?php } ?>
                            </select>
                            </div>
                          </div>
                        </div>
                        <div class="col-md-1">
                          <a class="btn btn-success" onclick="CatProT()"><i class="fa fa-plus"></i></a>
                        </div>
                        <div class="col-md-3">
                          <div class="form-group">
                            <label class="col-md-5 control-label">Stock mínimo:</label>
                            <div class=" col-md-7">
                              <input type="text" class="form-control" name="stockmin" value="<?php echo $stockmin;?>">
                            </div>
                          </div>
                        </div>
                        <div class="col-md-3">
                          <div class="form-group">
                            <label class="col-md-5 control-label">Existencias:</label>
                            <div class=" col-md-7">
                              <input type="text" class="form-control" name="stock" value="<?php echo $stock;?>">
                            </div>
                          </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <h3>Precios</h3>
                        <hr />
                        <div class="col-md-12">
                            <div class="form-group">
                              <label class="col-md-2 control-label">Precio Compra:</label>
                              <div class=" col-md-3">
                                <input type="number" name="preciocompra" id="preciocompraT" class="form-control" value="<?php echo $preciocompra;?>" >
                              </div>
                              <div class=" col-md-1" style="width: 47px;">
                                <button class="btn btn-raised">%</button>
                              </div>
                              <div class=" col-md-2">
                                <input type="number" class="form-control" name="porcentaje" id="porcentajeT"  aria-describedby="button-addon2" onchange="calcularT(this);" value="<?php echo $porcentaje;?>">
                              </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                              <label class="col-md-2 control-label">Precio venta:</label>
                              <div class=" col-md-3">
                                <input type="number" class="form-control" name="precioventa" id="precioventaT" value="<?php echo $precioventa;?>" placeholder="">
                              </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                              <label class="col-md-2 control-label">Precio mayoreo:</label>
                              <div class=" col-md-3">
                                <input type="number" class="form-control" name="mayoreo" id="preciomayoreoT" value="<?php echo $mayoreo;?>" placeholder="">
                              </div>
                              <div class=" col-md-2">
                                <input type="number" class="form-control" name="mayoreo" id="cpmayoreoT" value="<?php echo $canmayoreo;?>" placeholder="a partir de # de unidades">
                              </div>
                            </div>
                        </div>
                      </form>    
                    </div>
                  </div>
                  <div class="row" id="poveedoresT">
                    <div class="col-md-12">
                      <h3>Proveedores</h3>
                      <hr/>
                      <div class="row">
                        <div class="col-md-10" id="CTPT">
                          <form id="TRMP-0">
                            <input type="hidden" name="idd">
                            <div class="col-md-5 form-group">
                              <label>Proveedor</label>
                              <select class="form-control" id="prove" name="id_proveedor">
                              <?php foreach ($provedores as $key):?>
                                <option value="<?php echo $key->id_proveedor; ?>"><?php echo $key->razon_social; ?></option>
                              <?php endforeach ?>
                            </select>
                            </div>
                            <div class="col-md-5 form-group">
                              <label>Último Precio de compra</label>
                              <input class="form-control" type="text" name="precio">
                            </div>
                            <div class="col-md-2">
                              <br>
                              <button onclick="DeletePT(this)" type="button" class="btn btn-danger"><i class="fa fa-minus"></i></button>
                            </div>
                          </form>
                        </div>
                        <div class="col-md-2">
                          <br>
                          <button type="button" class="btn btn-raised btn-primary" onclick="CloneElementPT(0,0,'')">otro</button>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-12">
                    <button class="btn btn-raised gradient-purple-bliss white shadow-z-1-hover"  type="submit"  id="savepr">Guardar</button>
                  </div> 
              </div>
          </div>
      </div>
  </div>
</div>
<div class="modal fade text-left" id="Operacional" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="Titulo"></h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
           <div class="modal-body row">
            <div class="col-md-12 form-group">
              <label id="etiqueta"></label>
              <input class="form-control" type="text" name="NombreMed" id="nombreMed">
            </div>
            <div class="col-md-12">
              <a class="btn btn-success" id="CargarInf">Agregar</a>
            </div>
            <div class="col-md-12">
              <table class="table" id="tableop" style="width: 100%">
                <thead>
                  <th>Nombre</th>
                  <th>.</th>
                </thead>
              </table>
            </div>
           </div>
            <div class="modal-footer">
                <button type="button" class="btn grey btn-outline-secondary" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>
<div class="modal fade bd-example-modal-sm" id="ModalEdit" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-sm">
    <div class="modal-header">
      <h4 class="modal-title" id="Titulo">Editar</h4>
      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
    <div class="modal-content">
      <div class="modal-body row">
        <div class="col-md-12 form-group">
          <label>Nombre</label>
          <input class="form-control" type="text" name="nNombre">
        </div>
        <div class="col-md-2">
           <button type="button" class="btn btn-success" data-dismiss="modal">Editar</button>
        </div>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
  $(document).ready(function() {
          $('#generacodigo').click(function(event) {
            var d = new Date();
            var dia=d.getDate();//1 31
            var dias=d.getDay();//0 6
            var mes = d.getMonth();//0 11
            var yy = d.getFullYear();//9999
            var hr = d.getHours();//0 24
            var min = d.getMinutes();//0 59
            var seg = d.getSeconds();//0 59
            var yyy = 18;
            var ram = Math.floor((Math.random() * 10) + 1);
            var codigo=seg+''+min+''+hr+''+yyy+''+ram+''+mes+''+dia+''+dias;
            //var condigo0=condigo.substr(0,13);
            //console.log(codigo);
            $('#pcodigo').val(codigo);
          });
  });
</script>