<?php
$a=session_id();
if(empty($a)) session_start();
defined('BASEPATH') OR exit ('No direct script access allowed');

class ModeloProductos extends CI_Model {
    public function __construct() {
        parent::__construct();
    }
    function filas() {
        $strq = "SELECT COUNT(*) as total FROM productos where activo=1";
        $query = $this->db->query($strq);
        $this->db->close();
        foreach ($query->result() as $row) {
            $total =$row->total;
        } 
        return $total;
    }
    function total_paginados($por_pagina,$segmento) {
        //$consulta = $this->db->get('productos',$por_pagina,$segmento);
        //return $consulta;
        if ($segmento!='') {
            $segmento=','.$segmento;
        }else{
            $segmento='';
        }
        $strq = "SELECT pro.productoid,pro.nombre,pro.img,pro.codigo,pro.descripcion,cat.categoria,pro.precioventa,pro.stock FROM productos as pro
        inner join categoria as cat on cat.categoriaId=pro.categoria 
        where pro.activo=1 LIMIT $por_pagina $segmento";
        $query = $this->db->query($strq);
        $this->db->close();
        return $query;
    }
//======
    function productosall() {
        $strq = "SELECT pro.productoid,pro.nombre,pro.img,pro.codigo,pro.descripcion,cat.categoria,pro.precioventa,pro.stock FROM productos as pro
        inner join categoria as cat on cat.categoriaId=pro.categoria 
        where pro.activo=1";
        $query = $this->db->query($strq);
        $this->db->close();
        return $query;
    }
    function total_paginadosp($por_pagina,$segmento) {
        //$consulta = $this->db->get('productos',$por_pagina,$segmento);
        //return $consulta;
        if ($segmento!='') {
            $segmento=','.$segmento;
        }else{
            $segmento='';
        }
        $strq = "SELECT pro.productoid,pro.nombre,pro.img,pro.codigo FROM productos as pro
        inner join categoria as cat on cat.categoriaId=pro.categoria 
        where pro.activo=1 LIMIT $por_pagina $segmento";
        return $strq;
    }
    function productoallsearch($pro){
        $strq = "SELECT * FROM productos where activo=1 and codigo like '%".$pro."%' or activo=1 and nombre like '%".$pro."%'";
        $query = $this->db->query($strq);
        $this->db->close();
        return $query;
    }
    function categorias() {
        $strq = "SELECT * FROM categoria where activo=1";
        $query = $this->db->query($strq);
        $this->db->close();
        return $query;
    }
    public function productosinsert($cod,$pfiscal,$nom,$des,$catego,$stock,$preciocompra,$porcentaje,$precioventa,$pmmayoreo,$cpmmayoreo,$pmayoreo,$cpmayoreo,$stockmin){
            $strq = "INSERT INTO productos(codigo, productofiscal, nombre, descripcion, categoria, stock, preciocompra, porcentaje, precioventa, mediomayoreo, canmediomayoreo, mayoreo, canmayoreo,stockmin) 
                                   VALUES ('$cod',$pfiscal,'$nom','$des',$catego,$stock,'$preciocompra',$porcentaje,$precioventa,$pmmayoreo,$cpmmayoreo,$pmayoreo,$cpmayoreo,$stockmin)";
            $this->db->query($strq);
            $id=$this->db->insert_id();
            return $id;
    }
    public function productosupdate($id,$cod,$pfiscal,$nom,$des,$catego,$stock,$preciocompra,$porcentaje,$precioventa,$pmmayoreo,$cpmmayoreo,$pmayoreo,$cpmayoreo,$stockmin){
            $strq = "UPDATE productos SET codigo='$cod',productofiscal=$pfiscal,nombre='$nom',descripcion='$des',categoria=$catego,stock=$stock,preciocompra=$preciocompra,porcentaje=$porcentaje,precioventa=$precioventa,mediomayoreo=$pmmayoreo,canmediomayoreo=$cpmmayoreo,mayoreo=$pmayoreo,canmayoreo=$cpmayoreo,stockmin=$stockmin WHERE productoid=$id";
            $this->db->query($strq);
    }
    public function imgpro($file,$pro){
        $strq = "UPDATE productos SET img='$file' WHERE productoid=$pro";
        $this->db->query($strq);
    }
    function totalproductosenexistencia() {
        $strq = "SELECT ROUND(sum(stock),2) as total FROM `productos` where activo=1 ";
        $query = $this->db->query($strq);
        $this->db->close();
        foreach ($query->result() as $row) {
            $total =$row->total;
        } 
        return $total;
    }
    function totalproductopreciocompra() {
        $strq = "SELECT ROUND(sum(preciocompra),2) as total FROM `productos` where activo=1";
        $query = $this->db->query($strq);
        $this->db->close();
        foreach ($query->result() as $row) {
            $total =$row->total;
        } 
        return $total;
    }
    function totalproductoporpreciocompra(){
        $strq = "SELECT ROUND(sum(preciocompra*stock),2) as total FROM `productos` where activo=1";
        $query = $this->db->query($strq);
        $this->db->close();
        foreach ($query->result() as $row) {
            $total =$row->total;
        } 
        return $total;
    }
    function getproducto($id){
        $strq = "SELECT * FROM productos where productoid=$id";
        $query = $this->db->query($strq);
        $this->db->close();
        return $query;
    }
    function productosdelete($id){
        $strq = "UPDATE productos SET activo=0 WHERE productoid=$id";
        $this->db->query($strq);
    }
//=====
    function GetDataTablaT($id){
        $sql = "SELECT tm.id,tim.nombre FROM tipomed_prod AS tm INNER JOIN tipo_medica AS tim ON tm.id_tipom=tim.id WHERE status=1 AND tm.id_producto=".$id;
        $query = $this->db->query($sql);
        return $query->result();
    }

    function GetDataTablaS($id){
        $sql = "SELECT sp.id,sub.nombre FROM substancia_pro AS sp INNER JOIN substancia AS sub ON sp.id_substancia=sub.id WHERE status=1 AND sp.id_producto=".$id;
        $query = $this->db->query($sql);
        return $query->result();
    }

    function GetDataTablaC($id){
        $sql = "SELECT cp.id,cla.nombre FROM clasificacion_pro AS cp INNER JOIN clasificacion AS cla ON cp.id_clasificacion=cla.id WHERE status=1 AND cp.id_producto=".$id;
        $query = $this->db->query($sql);
        return $query->result();
    }

    function getlistproductos($params){
        $columns = array(
            0=>'pro.productoid',
            1=>'pro.nombre',
            2=>'cla.nombre',
            3=>'pro.precioventa',
            4=>'pro.stock',
            5=>'pro.codigo',
            6=>'pro.stockmin'
        );
        
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select("pro.nombre AS nopro,".$select);
        $this->db->from('productos pro');
        $this->db->join('clasificacion cla', 'cla.id=pro.clasificacion',"inner");
        $this->db->where(array('pro.activo'=>1, 'pro.tipopro'=>1));
        //$this->db->where(array('facturaabierta'=>1));
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }            
        $this->db->order_by($columns[$params['order'][0]['column']], $params['order'][0]['dir']);
        $this->db->limit($params['length'],$params['start']);
        //echo $this->db->get_compiled_select();
        $query=$this->db->get();
        // print_r($query); die;
        return $query;
    }

    function getlistproductosT($params){
        $columns = array(
            0=>'pro.productoid',
            1=>'pro.nombre',
            2=>'ca.categoria',
            3=>'pro.precioventa',
            4=>'pro.stock',
            5=>'pro.codigo',
            6=>'pro.stockmin'
        );
        
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select($select);
        $this->db->from('productos pro');
        $this->db->join('categoria ca', 'ca.categoriaId=pro.categoria',"inner");
        $this->db->where(array('pro.activo'=>1, 'pro.tipopro'=>2));
        //$this->db->where(array('facturaabierta'=>1));
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }            
        $this->db->order_by($columns[$params['order'][0]['column']], $params['order'][0]['dir']);
        $this->db->limit($params['length'],$params['start']);
        //echo $this->db->get_compiled_select();
        $query=$this->db->get();
        // print_r($query); die;
        return $query;
    }

    function getlistproductosS($params){
        $columns = array(
            0=>'pro.productoid',
            1=>'pro.nombre',
            2=>'pro.codigo',
        );
        
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select($select);
        $this->db->from('productos pro');
        $this->db->where(array('pro.activo'=>1, 'pro.tipopro'=>3));
        //$this->db->where(array('facturaabierta'=>1));
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }            
        $this->db->order_by($columns[$params['order'][0]['column']], $params['order'][0]['dir']);
        $this->db->limit($params['length'],$params['start']);
        //echo $this->db->get_compiled_select();
        $query=$this->db->get();
        // print_r($query); die;
        return $query;
    }

    function getlistproductosE($params){
        $columns = array(
            0=>'pro.productoid',
            1=>'pro.nombre',
            2=>'pro.tipopro',
            3=>'pro.precioventa',
            4=>'pro.stock',
            5=>'pro.codigo',
            6=>'pro.stockmin'
        );
        
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select($select);
        $this->db->from('productos pro');
        $this->db->where(array('pro.activo'=>1));
        $this->db->where('pro.stock <= pro.stockmin');
        $this->db->where('pro.tipopro!=3');
        //$this->db->where(array('facturaabierta'=>1));
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }            
        $this->db->order_by($columns[$params['order'][0]['column']], $params['order'][0]['dir']);
        $this->db->limit($params['length'],$params['start']);
        //echo $this->db->get_compiled_select();
        $query=$this->db->get();
        // print_r($query); die;
        return $query;
    }
}