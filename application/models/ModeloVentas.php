<?php
$a=session_id();
if(empty($a)) session_start();
defined('BASEPATH') OR exit ('No direct script access allowed');

class ModeloVentas extends CI_Model{ 
    public function __construct() {
        parent::__construct();
    }
    function ingresarventa($uss,$cli,$mpago,$sbtotal,$desc,$descu,$total,$efectivo,$tarjeta,$pagado){
    	$strq = "INSERT INTO ventas(id_personal, id_cliente, metodo, subtotal, descuento,descuentocant, monto_total,pagotarjeta,efectivo,pagado) 
                VALUES ($uss,$cli,$mpago,$sbtotal,$desc,$descu,$total,'$tarjeta','$efectivo','$pagado')";
        $query = $this->db->query($strq);
        $id=$this->db->insert_id();
        $this->db->close();
        return $id;
    }
    function ingresarventad($idventa,$producto,$cantidad,$precio){
    	$strq = "INSERT INTO venta_detalle(id_venta, id_producto, cantidad, precio) VALUES ($idventa,$producto,$cantidad,$precio)";
        $query = $this->db->query($strq);
        $this->db->close();

        $strq = "UPDATE productos SET stock=stock-$cantidad WHERE productoid=$producto";
        $query = $this->db->query($strq);
        $this->db->close();
    }
    function configticket(){
        $strq = "SELECT * FROM ticket";
        $query = $this->db->query($strq);
        $this->db->close();
        return $query;
    }
    function clientepordefecto(){
        $strq = "SELECT * FROM  clientes LIMIT 1";
        $query = $this->db->query($strq);
        $this->db->close();
        return $query;
    }
    function getventas($id){
        $strq = "SELECT * FROM ventas where id_venta=$id";
        $query = $this->db->query($strq);
        $this->db->close();
        return $query;
    }
    function getventasd($id){
        $strq = "SELECT vendell.cantidad, pro.nombre, vendell.precio
        FROM venta_detalle as vendell
        inner join productos as pro on pro.productoid=vendell.id_producto
        where vendell.id_venta=$id";
        $query = $this->db->query($strq);
        $this->db->close();
        return $query;
    }
    function ingresarcompra($uss,$prov,$total){
        $strq = "INSERT INTO compras(id_proveedor, monto_total) VALUES ($prov,$total)";
        $query = $this->db->query($strq);
        $id=$this->db->insert_id();
        $this->db->close();
        return $id;
    }
    function ingresarcomprad($idcompra,$producto,$cantidad,$precio){
        $strq = "INSERT INTO compra_detalle(id_compra, id_producto, cantidad, precio_compra) VALUES ($idcompra,$producto,$cantidad,$precio)";
        $query = $this->db->query($strq);
        $this->db->close();

        $strq = "UPDATE productos SET stock=stock+$cantidad WHERE productoid=$producto";
        $query = $this->db->query($strq);
        $this->db->close();
    }
    function turnos(){
        $strq = "SELECT * FROM turno ORDER BY id DESC LIMIT 1";
        $query = $this->db->query($strq);
        $this->db->close();
        $status='cerrado';
        foreach ($query->result() as $row) {
            $status =$row->status;
        }
        return $status;
    }
    function turnoss(){
        $strq = "SELECT * FROM turno ORDER BY id DESC LIMIT 1";
        $query = $this->db->query($strq);
        $this->db->close();
        return $query;
    }
    function abrirturno($cantidad,$nombre,$fecha,$horaa){
        $strq = "INSERT INTO turno(fecha, horaa, cantidad, nombre, status, user) VALUES ('$fecha','$horaa','$cantidad','$nombre','abierto','user')";
        $query = $this->db->query($strq);
        $this->db->close();
    }
    function cerrarturno($id,$horaa){
        $fechac=date('Y-m-d');
        $strq = "UPDATE turno SET horac='$horaa',fechacierre='$fechac', status='cerrado' WHERE id=$id";
        $query = $this->db->query($strq);
        $this->db->close();
    }
    function corte($inicio,$fin,$personal){
        if ($personal==0) {
            $whereus='';
        }else{
            $whereus='v.id_personal='.$personal.' and ';
        }

        $strq = "SELECT cl.ClientesId,cl.Nom, v.reg, v.id_venta, v.id_cliente, v.id_personal, v.monto_total,v.pagotarjeta,v.efectivo, v.subtotal, p.personalId, concat(p.nombre,p.apellidos) as vendedor, p.caja
                FROM ventas as v 
                left join personal as p on v.id_personal=p.personalId 
                left join usuarios as u on u.personalId=p.personalId
                join clientes as cl on cl.ClientesId=v.id_cliente
                WHERE $whereus v.reg between '$inicio 00:00:00' and '$fin 23:59:59' and v.cancelado=0 and v.metodo!=4 and v.pagado=1";
        $query = $this->db->query($strq);
        $this->db->close();
        return $query;
    }

    function corteCreditos($inicio,$fin,$personal){
        if ($personal==0) {
            $whereus='';
        }else{
            $whereus='v.id_personal='.$personal.' and ';
        }

        $strq = "SELECT cl.ClientesId,cl.Nom, v.reg, v.id_venta, v.id_cliente, v.id_personal, v.monto_total,v.pagotarjeta,v.efectivo, v.subtotal, p.personalId, concat(p.nombre,p.apellidos) as vendedor, p.caja
                FROM ventas as v 
                left join personal as p on v.id_personal=p.personalId 
                left join usuarios as u on u.personalId=p.personalId
                join clientes as cl on cl.ClientesId=v.id_cliente
                WHERE $whereus v.fecha_pago between '$inicio 00:00:00' and '$fin 23:59:59' and v.cancelado=0 and v.metodo=4 and v.pagado=1";
        $query = $this->db->query($strq);
        $this->db->close();
        return $query;
    }

    function cortevmm($inicio,$fin,$personal){
        if ($personal==0) {
            $whereus='';
        }else{
            $whereus='v.id_personal='.$personal.' and ';
        }

        $strq = "SELECT pr.nombre as producto,sum(vd.cantidad) as cantidad
                FROM ventas as v 
                join clientes as cl on cl.ClientesId=v.id_cliente
                join venta_detalle as vd on vd.id_venta=v.id_venta
                join productos as pr on pr.productoid=vd.id_producto
                left join personal as p on v.id_personal=p.personalId
                left join usuarios as u on u.personalId=p.personalId

                WHERE $whereus v.reg between '$inicio 00:00:00' and '$fin 23:59:59' and v.cancelado=0
                GROUP by vd.id_producto
                ORDER BY `cantidad` DESC
                limit 10";
                //log_message('error','checar'.$strq);
        $query = $this->db->query($strq);
        //$this->db->close();
        return $query;
    }


    function consultarturnoname($inicio,$fin){
        $strq = "SELECT * FROM turno where fecha>='$inicio' and fechacierre<='$fin'";
        $query = $this->db->query($strq);
        $this->db->close();
        //$nombre=$cfecha.'/'.$chora;
        return $query;
    }
    function cortesum($inicio,$fin,$personal){
        if ($personal==0) {
            $whereus='';
        }else{
            $whereus='ventas.id_personal='.$personal.' and ';
        }
        $strq = "SELECT sum(monto_total) as total , sum(subtotal) as subtotal, sum(descuentocant) as descuento 
                FROM ventas 
                where $whereus reg between '$inicio 00:00:00' and '$fin 23:59:59' and cancelado=0 and metodo!=4 and pagado=1";
        $query = $this->db->query($strq);
        $this->db->close();
        return $query;
    }

    function cortesumVC($inicio,$fin,$personal){
        if ($personal==0) {
            $whereus='';
        }else{
            $whereus='ventas.id_personal='.$personal.' and ';
        }
        $strq = "SELECT sum(monto_total) as total , sum(subtotal) as subtotal, sum(descuentocant) as descuento 
                FROM ventas 
                where $whereus fecha_pago between '$inicio 00:00:00' and '$fin 23:59:59' and cancelado=0 and metodo=4 and pagado=1";
        $query = $this->db->query($strq);
        $this->db->close();
        return $query;
    }

    function filas() {
        $strq = "SELECT COUNT(*) as total FROM ventas";
        $query = $this->db->query($strq);
        $this->db->close();
        foreach ($query->result() as $row) {
            $total =$row->total;
        } 
        return $total;
    }

    function corteEfectivo($inicio,$fin,$personal){
        if ($personal==0) {
            $whereus='';
        }else{
            $whereus='v.id_personal='.$personal.' and ';
        }
        $strq = "SELECT DISTINCT(v.id_venta), cl.ClientesId,cl.Nom, v.reg, v.id_venta, v.id_cliente, v.id_personal, v.monto_total,v.pagotarjeta,v.efectivo, v.subtotal, p.personalId, concat(p.nombre,p.apellidos) as vendedor, vd.cantidad,vd.precio,
            sum(vd.cantidad*vd.precio) as totalvd
                FROM ventas as v 
                left join personal as p on v.id_personal=p.personalId 
                join clientes as cl on cl.ClientesId=v.id_cliente
                join venta_detalle as vd on vd.id_venta=v.id_venta 
                WHERE $whereus v.reg between '$inicio 00:00:00' and '$fin 23:59:59' and v.cancelado!=1
                /*and efectivo > 0 and pagotarjeta=0*/ and metodo=1
                group by v.id_venta
                ORDER BY v.id_venta";
        $query = $this->db->query($strq);
        //$this->db->close();
        return $query;
    }
    function corteTarjeta($inicio,$fin,$personal){
        if ($personal==0) {
            $whereus='';
        }else{
            $whereus='v.id_personal='.$personal.' and ';
        }
        $strq = "SELECT DISTINCT(v.id_venta), cl.ClientesId,cl.Nom, v.reg, v.id_venta, v.id_cliente, v.id_personal, v.monto_total,v.pagotarjeta,v.efectivo, v.subtotal, p.personalId, concat(p.nombre,p.apellidos) as vendedor, vd.cantidad,vd.precio,
            sum(vd.cantidad*vd.precio) as totalvd
                FROM ventas as v 
                left join personal as p on v.id_personal=p.personalId 
                join clientes as cl on cl.ClientesId=v.id_cliente
                join venta_detalle as vd on vd.id_venta=v.id_venta 

                WHERE $whereus v.reg between '$inicio 00:00:00' and '$fin 23:59:59' and v.cancelado!=1
                /*and efectivo = 0 and pagotarjeta>0*/ and metodo!=1 and metodo!=4
                group by v.id_venta
                ORDER BY v.id_venta";
        $query = $this->db->query($strq);
        return $query;
    }


    function total_paginados($por_pagina,$segmento) {
        //$consulta = $this->db->get('productos',$por_pagina,$segmento);
        //return $consulta;
        if ($segmento!='') {
            $segmento=','.$segmento;
        }else{
            $segmento='';
        }
        $strq = "SELECT ven.id_venta,ven.reg, cli.Nom as cliente, concat(per.nombre,' ',per.apellidos) as vendedor,ven.metodo,ven.subtotal, ven.monto_total,ven.cancelado
                FROM ventas as ven 
                inner join personal as per on per.personalId=ven.id_personal
                inner join clientes as cli on cli.ClientesId=ven.id_cliente
                ORDER BY ven.id_venta DESC
                LIMIT $por_pagina $segmento";
        $query = $this->db->query($strq);
        $this->db->close();
        return $query;
    }
    function filastur() {
        $strq = "SELECT COUNT(*) as total FROM turno";
        $query = $this->db->query($strq);
        $this->db->close();
        foreach ($query->result() as $row) {
            $total =$row->total;
        } 
        return $total;
    }
    function cancalarventa($id){
        $fecha = date('Y-m-d');
        $strq = "UPDATE ventas SET cancelado=1,hcancelacion='$fecha' WHERE `id_venta`=$id";
        $query = $this->db->query($strq);
        $this->db->close();
    }
    function ventadetalles($id){
        $strq = "SELECT * FROM venta_detalle where id_venta=$id";
        $query = $this->db->query($strq);
        $this->db->close();
        return $query;
    }
    function regresarpro($id,$can){
        $strq = "UPDATE productos set stock=stock+$can where productoid=$id";
        $query = $this->db->query($strq);
        $this->db->close();
    }
    function filasturnos() {
        $strq = "SELECT COUNT(*) as total FROM turno";
        $query = $this->db->query($strq);
        $this->db->close();
        foreach ($query->result() as $row) {
            $total =$row->total;
        } 
        return $total;
    }
    function total_paginadosturnos($por_pagina,$segmento) {
        //$consulta = $this->db->get('productos',$por_pagina,$segmento);
        //return $consulta;
        if ($segmento!='') {
            $segmento=','.$segmento;
        }else{
            $segmento='';
        }
        $strq = "SELECT * FROM turno ORDER BY id DESC
                LIMIT $por_pagina $segmento";
        $query = $this->db->query($strq);
        $this->db->close();
        return $query;
    }
    function consultarturno($id){
        $strq = "SELECT * FROM turno where id=$id";
        $query = $this->db->query($strq);
        $this->db->close();
        return $query;
    }
    function consultartotalturno($fecha,$horaa,$horac,$fechac,$personal){
        if ($personal==0) {
            $whereus='';
        }else{
            $whereus='v.id_personal='.$personal.' and ';
        }
        $strq = "SELECT sum(monto_total) as total FROM ventas where $whereus reg between '$fecha $horaa' and '$fechac $horac' and cancelado=0";
        $query = $this->db->query($strq);
        $this->db->close();
        foreach ($query->result() as $row) {
            $total =$row->total;
        }
        return $total;
    }
    function consultartotalturno2($fecha,$horaa,$horac,$fechac,$personal){
        if ($personal==0) {
            $whereus='';
        }else{
            $whereus='v.id_personal='.$personal.' and ';
        }
        $strq = "SELECT sum(p.preciocompra*vd.cantidad) as preciocompra 
                from venta_detalle as vd
                inner join productos as p on vd.id_producto=p.productoid
                inner join ventas as v on vd.id_venta=v.id_venta
                where $whereus v.cancelado=0 and v.reg>='$fecha $horaa' and v.reg<='$fechac $horac'";
        $query = $this->db->query($strq);
        $this->db->close();
        foreach ($query->result() as $row) {
            $total =$row->preciocompra;
        }
        return $total;
    }

    function consultartotalturno1($fecha,$fechaf,$personal){
        if ($personal==0) {
            $whereus='';
        }else{
            $whereus='ventas.id_personal='.$personal.' and ';
        }
        $strq = "SELECT sum(monto_total) as total FROM ventas where $whereus reg between '$fecha 00:00:00' and '$fechaf 23:59:59' and cancelado=0 and pagado=1";
        $query = $this->db->query($strq);
        $this->db->close();
        foreach ($query->result() as $row) {
            $total =$row->total;
        }
        return $total;
    }
    function consultartotalturno2_1($fecha,$fechaf,$personal){
        if ($personal==0) {
            $whereus='';
        }else{
            $whereus='v.id_personal='.$personal.' and ';
        }
        $strq = "SELECT sum(p.preciocompra*vd.cantidad) as preciocompra 
                from venta_detalle as vd
                inner join productos as p on vd.id_producto=p.productoid
                inner join ventas as v on vd.id_venta=v.id_venta
                where $whereus v.cancelado=0 and v.reg>='$fecha 00:00:00' and v.reg<='$fechaf 23:59:59' and pagado=1";
        $query = $this->db->query($strq);
        $this->db->close();
        foreach ($query->result() as $row) {
            $total =$row->preciocompra;
        }
        return $total;
    }


    function consultartotalturnopro($fecha,$horaa,$horac,$fechac){
        $strq = "SELECT p.nombre as producto,vd.cantidad, vd.precio
                from venta_detalle as vd
                inner join productos as p on vd.id_producto=p.productoid
                inner join ventas as v on vd.id_venta=v.id_venta
                where v.cancelado=0 and v.reg>='$fecha $horaa' and v.reg<='$fechac $horac'";
        $query = $this->db->query($strq);
        $this->db->close();
        return $query;
    }
    function consultartotalturnopromas($fecha,$horaa,$horac,$fechac){
        $strq = "SELECT p.nombre as producto, sum(vd.cantidad) as total 
                from venta_detalle as vd 
                inner join productos as p on vd.id_producto=p.productoid 
                inner join ventas as v on vd.id_venta=v.id_venta 
                where v.cancelado=0 and v.reg>='$fecha $horaa' and v.reg<='$fechac $horac' GROUP BY producto ORDER BY `total` DESC LIMIT 1";
        $query = $this->db->query($strq);
        $this->db->close();
        return $query;
    }

    function filaslcompras(){
        $strq = "SELECT COUNT(*) as total FROM compra_detalle WHERE activo=1";
        $query = $this->db->query($strq);
        $this->db->close();
        foreach ($query->result() as $row) {
            $total =$row->total;
        } 
        return $total;
    }

    function total_paginadoslcompras($por_pagina,$segmento) {
        //$consulta = $this->db->get('productos',$por_pagina,$segmento);
        //return $consulta;
        if ($segmento!='') {
            $segmento=','.$segmento;
        }else{
            $segmento='';
        }
        $strq = "SELECT compdll.id_detalle_compra,comp.reg,pro.nombre as producto,prov.razon_social,compdll.cantidad,compdll.precio_compra 
                FROM compra_detalle as compdll 
                inner join compras as comp on comp.id_compra=compdll.id_compra 
                inner join productos as pro on pro.productoid=compdll.id_producto 
                inner join proveedores as prov on prov.id_proveedor=comp.id_proveedor
                WHERE  compdll.activo=1
                ORDER BY compdll.id_detalle_compra DESC
                LIMIT $por_pagina $segmento";
        $query = $this->db->query($strq);
        $this->db->close();
        return $query;
    }
    function lcomprasconsultar($inicio,$fin) {
        
        $strq = "SELECT compdll.id_detalle_compra,comp.reg,pro.nombre as producto,prov.razon_social,compdll.cantidad,compdll.precio_compra 
                FROM compra_detalle as compdll 
                inner join compras as comp on comp.id_compra=compdll.id_compra 
                inner join productos as pro on pro.productoid=compdll.id_producto 
                inner join proveedores as prov on prov.id_proveedor=comp.id_proveedor 
                where comp.reg>='$inicio 00:00:00' and comp.reg<='$fin 23:59:59'
                ORDER BY compdll.id_detalle_compra DESC";
        $query = $this->db->query($strq);
        $this->db->close();
        return $query;
    }
    function ventassearch($search){
        $strq = "SELECT ven.id_venta,ven.reg, cli.Nom as cliente, concat(per.nombre,' ',per.apellidos) as vendedor,ven.metodo,ven.subtotal, ven.monto_total,ven.cancelado
                FROM ventas as ven 
                inner join personal as per on per.personalId=ven.id_personal
                inner join clientes as cli on cli.ClientesId=ven.id_cliente
                where ven.id_venta like '%".$search."%' or 
                      ven.reg like '%".$search."%' or
                      cli.Nom like '%".$search."%' or
                      per.nombre like '%".$search."%' or
                      ven.monto_total like '%".$search."%'
                ";
        $query = $this->db->query($strq);
        $this->db->close();
        return $query;
    }

    function GetTableProductos($id){
        $sql="SELECT vd.cantidad, pro.nombre, pro.precioventa FROM venta_detalle AS vd INNER JOIN productos AS pro ON pro.productoid=vd.id_producto WHERE vd.id_venta=".$id;
        $query = $this->db->query($sql);
        return $query->result();
    }
 
    function GetDataCredito(){
        $SQL="SELECT cl.ClientesId AS id, cl.Nom, SUM(v.monto_total) AS mont FROM clientes AS cl INNER JOIN ventas AS v ON cl.ClientesId=v.id_cliente WHERE v.metodo AND v.pagado=0 GROUP BY cl.ClientesId";
        $query = $this->db->query($SQL);
        return $query->result();
    }
}