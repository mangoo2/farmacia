<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ModeloGeneral extends CI_Model {
	
	public function __construct(){
		parent::__construct();
		//Do your magic here
	}

	public function get_table($table){
    	$sql = "SELECT * FROM $table";
        $query = $this->db->query($sql);
        return $query->result();
    }

    public function get_table2($table){
        $sql = "SELECT * FROM $table";
        $query = $this->db->query($sql);
        return $query;
    }

    public function get_table_active($table){
    	$sql = "SELECT * FROM $table WHERE activo=1";
        $query = $this->db->query($sql);
        return $query->result();
    }

    public function get_table_activeProvee($table){
        $sql = "SELECT * FROM $table WHERE activo=1 ORDER BY alias ASC";
        $query = $this->db->query($sql);
        return $query->result();
    }

    public function get_table_activePT($table){
        $sql = "SELECT * FROM $table WHERE activo=1 and tipo!=3";
        $query = $this->db->query($sql);
        return $query->result();
    }

    public function GetTablaConN($condition,$table){
    $sql = "SELECT *, CONCAT(nombre,' ',apellidoP,' ',apellidoM) AS Cnombre FROM $table WHERE $condition";
        $query = $this->db->query($sql);
        return $query->result();
    }

    public function get_table_activePTM($table){
        $sql = "SELECT * FROM $table WHERE activo=1 and tipo=3";
        $query = $this->db->query($sql);
        return $query->result();
    }

    public function get_table_Marca($table){
        $sql = "SELECT * FROM $table WHERE activo=1";
        $query = $this->db->query($sql);
        return $query->result();
    }

    public function get_table_activee($table){
        $sql = "SELECT * FROM $table WHERE activo=1";
        $query = $this->db->query($sql);
        return $query->result();
    }

    public function get_table_active2($table){ //usuarios con cuenta
        $sql = "SELECT * FROM $table WHERE activo=1 and usuario!='' and password!='' ";
        $query = $this->db->query($sql);
        return $query->result();
    }

    public function get_table_active3($table){ //empleados sin cuenta
        $sql = "SELECT * FROM $table WHERE activo=1 and usuario='' and password='' ";
        $query = $this->db->query($sql);
        return $query->result();
    }

    public function get_record($id,$table){
    	$sql = "SELECT * FROM $table WHERE id=$id";
        $query = $this->db->query($sql);
        return $query->row();
    }

    public function get_recordW($where,$table){
        $sql = "SELECT * FROM $table WHERE $where";
        $query = $this->db->query($sql);
        return $query->row();
    }

    public function get_recordWeAll($where,$table){
        $sql = "SELECT * FROM $table WHERE $where";
        $query = $this->db->query($sql);
        return $query->result();
    }

    public function get_recordWeAllCat($where,$table){
        $sql = "SELECT ta.categoriaId AS id, ta.categoria AS nombre, ta.activo FROM $table AS ta WHERE $where";
        $query = $this->db->query($sql);
        return $query->result();
    }

    function GetAllid($id,$tabla){
        $sql="SELECT * FROM $tabla WHERE id=".$id;
        $query=$this->db->query($sql);
        return $query->result();    
    }

    public function get_recordFor($id,$table){
        $sql = "SELECT * FROM $table WHERE id=$id";
        $query = $this->db->query($sql);
        return $query;
    }

    public function get_recordVen($id,$table){
        $this->db->select('*');
        $this->db->from($table);
        $this->db->where('venta_id',$id);
        $query = $this->db->get();
        if($query->num_rows()> 0)
        {
          return $query;
        }
        else
        {
          return $query;  
        }
    }

    public function get_records_condition($condition,$table){
    	$sql = "SELECT * FROM $table WHERE $condition";
        $query = $this->db->query($sql);
        return $query->result();
    }

    public function get_records_conditionMarcas($condition,$table){
        $sql = "SELECT * FROM $table WHERE $condition ORDER BY nombre ASC";
        $query = $this->db->query($sql);
        return $query->result();
    }

    public function add_record($data,$table){
    	$this->db->insert($table, $data);
        return $this->db->insert_id();
    }

    public function edit_record($id,$cc,$data,$table){
    	$this->db->set($data);
        $this->db->where($cc, $id);
        return $this->db->update($table);
    }

    public function edit_recordW($table,$data,$where){
        $this->db->set($data);
        $this->db->where($where);
        return $this->db->update($table);
    }

    public function updateDate($id,$date,$table)
    {
        $this->db->where('id', $id);
        $this->db->set('fecha_reg' , $date);
        $this->db->update($table);
    }

    public function delete_records($condition,$table){
    	$sql = "DELETE FROM $table WHERE $condition";
        $query = $this->db->query($sql);
        return $query;
    }

    public function LogicDelete($table,$id){
        $strq = "UPDATE $table SET activo=0 WHERE id=$id";
        $query = $this->db->query($strq);
    }

    public function SimpleGet($valor,$campo,$tabla,$cont){
        $this->db->select($cont);
        $this->db->where($campo,$valor);
        return $this->db->get($tabla)->row()->$cont;
    }

    public function SimpleGetC($tabla,$where,$cont){
        $this->db->select($cont);
        $this->db->where($where);
        return $this->db->get($tabla)->row()->$cont;
    }

    public function getWhere($tabla,$where){
        $this->db->select("*");
        $this->db->from($tabla);
        $this->db->where($where);
        $query = $this->db->get();
        return $query->row();
    }

    public function getWhereRes($tabla,$where){
        $this->db->select("*");
        $this->db->from($tabla);
        $this->db->where($where);
        $query = $this->db->get();
        return $query->result();
    }

    public function getWhereResCat($table,$where){
        $sql = "SELECT ta.categoriaId AS id, ta.categoria AS nombre, ta.activo FROM $table AS ta WHERE $where";
        $query = $this->db->query($sql);
        return $query->result();
    }

    function unidadaddimg($img,$id){
        $strq = "UPDATE unidades SET tarjeta='$img' WHERE id=$id";
        $query = $this->db->query($strq);
        $this->db->close();
    }

    public function get_select_like_pais($buscar) {
        $strq = "SELECT * FROM pais where pais like '%$buscar%' or clave like '%$buscar%'";
        $query = $this->db->query($strq);
        return $query->result();
    }

    public function GenUpdateW($data,$table,$where){
        $this->db->where($where);
        $this->db->update($table,$data);
        return $this->db->affected_rows();
    }

    public function GenContar($campo,$dato,$tabla){
        $this->db->like($campo, $dato);
        $this->db->from($tabla);
        return $this->db->count_all_results();
        //$this->db->last_query();
    }

    public function GenContar2($where,$tabla){
        $this->db->where($where);
        $this->db->from($tabla);
        return $this->db->count_all_results();
        //$this->db->last_query();
    }

    public function get_docsTitular($table,$where,$col){
        $this->db->select('IFNULL('.$col.', "0") as '.$col);
        $this->db->from($table);
        $this->db->where($where);
        $query=$this->db->get();
        return $query->result();
    }

    public function updateCatalogo($data,$idname,$id,$catalogo){
        $this->db->set($data);
        $this->db->where($idname, $id);/// Se ocupa para n select
        $this->db->update($catalogo);
        return $id;
    }
	

}

/* End of file ModeloGeneral.php */
/* Location: ./application/models/ModeloGeneral.php */