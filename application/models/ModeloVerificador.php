<?php
$a=session_id();
if(empty($a)) session_start();
defined('BASEPATH') OR exit ('No direct script access allowed');

class ModeloVerificador extends CI_Model {
    public function __construct() {
        parent::__construct();
    }
    
    public function getDatos($prod){
        $this->db->select("p.nombre, p.productoid, s.nombre as substancia, p.substancia as idsubstancia, p.precioventa, p.stock, p.codigo");
        $this->db->from("productos p");
        $this->db->join("substancia s","s.id=p.substancia","left");
        $this->db->where("activo",1);
        $this->db->like("p.nombre",$prod);
        $this->db->or_like("p.codigo",$prod);
        $this->db->or_like("s.nombre",$prod);

        $query=$this->db->get();
        return $query->result();
    }

    public function get_similaresProd($id,$subs,$idsubs){
        $this->db->select("p.*, c.categoria, s.nombre as substancia");
        $this->db->from("productos p");
    
        $this->db->join("categoria c","c.categoriaId=p.categoria","left");
        $this->db->join("substancia s","s.id=p.substancia","left");
        $this->db->where("p.productoid",$id);
        $query1 = $this->db->get_compiled_select(); 

        $this->db->select("p.*, c.categoria, s.nombre as substancia");
        $this->db->from("productos p");
        $this->db->join("substancia s","s.id=p.substancia","left");
        $this->db->join("categoria c","c.categoriaId=p.categoria","left");
        //$this->db->like("p.substancia",$subs);
        //$this->db->where("p.productoid",$id);
        if($idsubs!=""){
            $this->db->where("p.substancia",$idsubs);
        }
        $this->db->where("p.activo",1);
        //$this->db->where("ps.activo",1);

        $query2 = $this->db->get_compiled_select(); 

        $this->db->select("p.*, c.categoria, s.nombre as substancia");
        $this->db->from("producto_similar ps");
        $this->db->join("productos p","p.productoid=ps.similar");
        $this->db->join("categoria c","c.categoriaId=p.categoria","left");
        $this->db->join("substancia s","s.id=p.substancia","left");
        $this->db->where("ps.id_producto",$id);
        $this->db->where("ps.activo",1);
        $query3 = $this->db->get_compiled_select(); 

        return $this->db->query($query1." UNION ".$query2." UNION ".$query3)->result();
    }
   

}